FROM node:latest as node

# Set the working directory
WORKDIR /app

# Add the source code to app
COPY . .

# Install all the dependencies
RUN npm install && npm run ng build

# Stage 2: Serve app with nginx server

# Use official nginx image as the base image
FROM nginx:alpine

WORKDIR /usr/share/nginx/html

RUN rm -rf ./*

COPY --from=node /app/dist/frontend/ .

ENTRYPOINT ["nginx","-g","daemon off;"]

# Copy our custom nginx config
#COPY nginx.conf /etc/nginx/nginx.conf