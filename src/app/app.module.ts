import { BrowserModule } from "@angular/platform-browser";
import { APP_INITIALIZER, NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AgmCoreModule } from "@agm/core";
import { HttpService } from "./shared/services/http.service";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import { TokenInterceptor } from "./shared/interceptors/token.interceptor";
import { LoggedGuard } from "./shared/guards/logged.guard";
import { NotLoggedGuard } from "./shared/guards/not-logged.guard";
import { AcceptedApprovalsDirective } from "./shared/validators/accepted-approvals.directive";
import { CoreModule } from "./core/core.module";
import { SharedModule } from "./shared/shared.module";
import { DateFormatPipe } from "./shared/pipes/date-format.pipe";
import { StoreService } from "./shared/services/store.service";
import { ProductComponent } from "./features/product/product.component";

export function initStoreData(storeService: StoreService) {
  return () => storeService.initStoreData();
}

@NgModule({
  declarations: [AppComponent, ProductComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyB-uKlMO6BNy0D-ARQGbbrOrHY1cmpw7C0",
    }),
    HttpClientModule,
  ],
  providers: [
    HttpService,
    CookieService,
    LoggedGuard,
    NotLoggedGuard,
    AcceptedApprovalsDirective,
    DateFormatPipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
    StoreService,
    {
      provide: APP_INITIALIZER,
      useFactory: initStoreData,
      multi: true,
      deps: [StoreService],
    },
  ],

  bootstrap: [AppComponent],
})
export class AppModule {}
