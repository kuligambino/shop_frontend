export class StoreData {
  constructor(
    public emailPattern?: string,
    public phoneNumberPattern?: string,
    public postCodePattern?: string,
    public passwordPattern?: string,
    public birthDatePattern?: string,
    public creditCardPattern?: string,
    public cvvPattern?: string,
    public nipPattern?: string
  ) {}
}
