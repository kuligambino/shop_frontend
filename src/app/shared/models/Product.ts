export class Product {
  constructor(
    public id?: number,
    public name?: string,
    public weight?: number,
    public price?: number,
    public taste?: string,
    public categoryName?: string,
    public img?: string
  ) {}
}
