import { Directive, HostListener } from "@angular/core";

@Directive({
  selector: "[appPostCodeInput]",
})
export class PostCodeInputDirective {
  @HostListener("input", ["$event"])
  onKeyDown(event: KeyboardEvent) {
    const input = event.target as HTMLInputElement;

    let trimmed = input.value.replace(/\s+/g, "");

    if (trimmed.length > 6) {
      trimmed = trimmed.substr(0, 6);
    }

    trimmed = trimmed.replace(/-/g, "");

    let numbers = [];
    numbers.push(trimmed.substr(0, 2));
    if (trimmed.substr(2, 3) !== "") {
      numbers.push(trimmed.substr(2, 3));
    }
    input.value = numbers.join("-");
  }
}
