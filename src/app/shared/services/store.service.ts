import { Injectable } from "@angular/core";
import { HttpService } from "./http.service";
import { StoreData } from "../models/StoreData";

@Injectable({
  providedIn: "root",
})
export class StoreService {
  storeData: StoreData = new StoreData();

  constructor(private httpService: HttpService) {}

  initStoreData(): Promise<StoreData> {
    return this.httpService
      .getStoreData()
      .toPromise()
      .then((storeData: StoreData) => {
        this.storeData = storeData;
        return storeData;
      });
  }
}
