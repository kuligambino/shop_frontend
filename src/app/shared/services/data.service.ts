import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Order } from "../../features/order/model/Order";
import { Product } from "../models/Product";

@Injectable({
  providedIn: "root",
})
export class DataService {
  private userEmailSource = new BehaviorSubject("default");
  private orderSource = new BehaviorSubject({});
  private addedProductSource = new BehaviorSubject({});

  currentEmail = this.userEmailSource.asObservable();
  currentOrder = this.orderSource.asObservable();
  addedProduct = this.addedProductSource.asObservable();

  changeEmail(email: string): void {
    this.userEmailSource.next(email);
  }

  emitOrder(order: Order): void {
    this.orderSource.next(order);
  }

  addProduct(product: Product) {
    this.addedProductSource.next(product);
  }
}
