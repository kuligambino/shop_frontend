import { Injectable } from "@angular/core";
import { Order } from "../../features/order/model/Order";
import { CookieService } from "ngx-cookie-service";
import { HttpService } from "./http.service";
import { QuantityUpdateService } from "./quantity-update.service";
import { DataService } from "./data.service";

@Injectable({
  providedIn: "root",
})
export class OrderService {
  constructor(
    private cookieService: CookieService,
    private httpService: HttpService,
    private quantityUpdateService: QuantityUpdateService,
    private dataService: DataService
  ) {}

  addToOrder(productId: number, quantity?: number) {
    let orderId;
    const orderExists = this.cookieService.check("order");
    if (orderExists) {
      orderId = this.cookieService.get("order");
      this.httpService
        .addToOrder(productId, orderId, quantity)
        .subscribe((order: Order) => {
          const product = order.orderProducts.filter(
            (p) => p.id === productId
          )[0];
          this.dataService.addProduct(product);
          this.quantityUpdateService.triggerQuantity();
        });
    } else {
      this.httpService
        .addToOrder(productId, null, quantity)
        .subscribe((order: Order) => {
          this.cookieService.set("order", order.id.toString());
          const product = order.orderProducts.filter(
            (p) => p.id === productId
          )[0];
          this.dataService.addProduct(product);
          this.quantityUpdateService.triggerQuantity();
        });
    }
  }

  getCookie(name: string): string {
    const cookieExists = this.cookieService.check(name);
    if (cookieExists) {
      return this.cookieService.get(name);
    }
  }

  saveCookie(name: string, value: string): void {
    this.cookieService.set(name, value);
  }

  deleteCookie(name: string): void {
    this.cookieService.delete(name);
  }
}
