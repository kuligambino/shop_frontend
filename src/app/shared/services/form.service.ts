import { Injectable } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AcceptedApprovalsDirective } from "../validators/accepted-approvals.directive";
import { StoreService } from "./store.service";
import { User } from "../models/User";
import { HttpService } from "./http.service";
import { OldPasswordValidator } from "../validators/old-password-validator";
import { Address } from "../../features/order/check-out2-step/models/Address";
import { EmailExistsValidator } from "../validators/email-exists-validator";

@Injectable({
  providedIn: "root",
})
export class FormService {
  passwordPattern = this.storeService.storeData.passwordPattern;
  emailPattern = this.storeService.storeData.emailPattern;
  creditCardRegex = this.storeService.storeData.creditCardPattern;
  cvvRegex = this.storeService.storeData.cvvPattern;
  datePattern = this.storeService.storeData.birthDatePattern;
  phoneRegex = this.storeService.storeData.phoneNumberPattern;
  postCodeRegex = this.storeService.storeData.postCodePattern;
  nipRegex = this.storeService.storeData.nipPattern;

  constructor(
    private readonly storeService: StoreService,
    private readonly formBuilder: FormBuilder,
    private readonly approvalValidator: AcceptedApprovalsDirective,
    private readonly httpService: HttpService
  ) {}

  initRegisterForm(): FormGroup {
    return this.formBuilder.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      email: ["", [Validators.required, Validators.pattern(this.emailPattern)]],
      password: [
        "",
        {
          updateOn: "change",
          validators: [
            Validators.required,
            Validators.pattern(this.passwordPattern),
          ],
        },
      ],
      confirmedPassword: ["", Validators.required],
      phoneNumber: "",
      birthDate: ["", Validators.required],
      recaptcha: [null, Validators.required],
      approvals: this.formBuilder.array([], {
        updateOn: "blur",
        validators: [this.approvalValidator.validate],
      }),
    });
  }

  initLoginForm(): FormGroup {
    return this.formBuilder.group({
      email: ["", [Validators.required, Validators.pattern(this.emailPattern)]],
      password: ["", Validators.required],
    });
  }

  initContactForm(): FormGroup {
    return this.formBuilder.group({
      email: ["", [Validators.required, Validators.pattern(this.emailPattern)]],
      details: ["", [Validators.required, Validators.minLength(50)]],
      grade: [5, Validators.required],
      recaptcha: [null, Validators.required],
    });
  }

  initPaymentForm(): FormGroup {
    return this.formBuilder.group(
      {
        ownerFirstName: ["", Validators.required],
        ownerLastName: ["", Validators.required],
        cardNumber: [
          "",
          [Validators.required, Validators.pattern(this.creditCardRegex)],
        ],
        expirationMonth: ["", Validators.required],
        expirationYear: ["", Validators.required],
        cvv: ["", [Validators.required, Validators.pattern(this.cvvRegex)]],
      },
      { updateOn: "blur" }
    );
  }

  initPersonalDataForm(user: User): FormGroup {
    return this.formBuilder.group(
      {
        firstName: [user.firstName, Validators.required],
        lastName: [user.lastName, Validators.required],
        email: [{ value: user.email, disabled: true }],
        phoneNumber: [
          user.phoneNumber,
          [Validators.maxLength(9), Validators.minLength(9)],
        ],
        birthDate: [
          user.birthDate,
          [Validators.required, Validators.pattern(this.datePattern)],
        ],
      },
      { updateOn: "blur" }
    );
  }

  initResetPasswordForm(): FormGroup {
    return this.formBuilder.group(
      {
        oldPassword: [
          "",
          Validators.required,
          OldPasswordValidator.isOldPasswordSame(this.httpService),
        ],
        newPassword: [
          "",
          {
            validators: [
              Validators.required,
              Validators.pattern(this.passwordPattern),
            ],
            updateOn: "change",
          },
        ],
        repeatedNewPassword: ["", Validators.required],
      },
      { updateOn: "blur" }
    );
  }

  initDelivery(address: Address): FormGroup {
    return this.formBuilder.group(
      {
        firstName: [address.firstName, Validators.required],
        lastName: [address.lastName, Validators.required],
        email: [
          address.email,
          [Validators.required, Validators.pattern(this.emailPattern)],
        ],
        phoneNumber: [
          address.phoneNumber,
          // [Validators.required, Validators.pattern(this.phoneRegex)],
        ],
        street: [address.street, Validators.required],
        city: [address.city, Validators.required],
        postCode: [
          address.postCode,
          [Validators.required, Validators.pattern(this.postCodeRegex)],
        ],
        country: [address.country, Validators.required],
      },
      { updateOn: "blur" }
    );
  }

  initForgetPasswordForm(): FormGroup {
    return this.formBuilder.group(
      {
        userEmail: [
          "",
          Validators.required,
          EmailExistsValidator.emailExists(this.httpService),
        ],
        recaptchaAccepted: [null, Validators.required],
      },
      { updateOn: "blur" }
    );
  }

  initAddressForm(address?: Address): FormGroup {
    return this.formBuilder.group(
      {
        id: address?.id,
        street: [address?.street || "", Validators.required],
        city: [address?.city || "", Validators.required],
        postCode: [
          address?.postCode || "",
          [Validators.required, Validators.pattern(this.postCodeRegex)],
        ],
        country: [address?.country || "", Validators.required],
      },
      { updateOn: "blur" }
    );
  }

  initAddressesForm(): FormGroup {
    return this.formBuilder.group({
      addresses: this.formBuilder.array([]),
    });
  }

  initInvoiceForm(): FormGroup {
    return this.formBuilder.group({
      companyName: ["", Validators.required],
      nip: ["", [Validators.required, Validators.pattern(this.nipRegex)]],
      companyAddress: ["", Validators.required],
    });
  }
}
