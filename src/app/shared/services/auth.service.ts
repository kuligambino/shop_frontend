import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { HOME_PAGE_URL } from "../../global_properties";
import { CookieService } from "ngx-cookie-service";
import { HttpService } from "./http.service";
import { LoginForm } from "src/app/features/login/LoginForm";
import {
  HEADER_STRING,
  JWT_TOKEN,
  REPLACE_VALUE,
  TOKEN_PREFIX,
} from "../constants";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(
    private cookieService: CookieService,
    private httpService: HttpService
  ) {}

  saveTokenToCookies(response: HttpResponse<LoginForm>) {
    const auth = response.headers.get(HEADER_STRING);
    const token = auth.replace(TOKEN_PREFIX, REPLACE_VALUE);
    this.cookieService.set(JWT_TOKEN, token);
  }

  saveOrderToCookies(response: HttpResponse<any>) {
    const orderId = response.headers.get("order");
    if (orderId !== null) {
      this.cookieService.set("order", orderId);
    }
  }

  isAuthenticated(): boolean {
    return !!this.getJwToken();
  }

  getJwToken(): string {
    return this.cookieService.get(JWT_TOKEN);
  }

  getOrderId(): string {
    return this.cookieService.get("order");
  }

  logout(): void {
    this.cookieService.delete(JWT_TOKEN);
    this.cookieService.delete("order");
    this.httpService.redirectToPage(HOME_PAGE_URL);
  }
}
