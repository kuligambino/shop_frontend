import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpResponse } from "@angular/common/http";
import { Contact } from "src/app/features/contact/Contact";
import { Observable } from "rxjs/internal/Observable";
import { RegisterForm } from "../../features/register/models/RegisterForm";
import { Router } from "@angular/router";

import {
  ADD_ADDRESS,
  ADD_ADDRESS1,
  ADD_TO_ORDER_URL,
  ADDRESS,
  ADDRESSES,
  ADDRESSES1,
  APPROVALS_URL,
  CHANGE_PASSWORD_FORM_URL,
  CONTACT_FORM_URL,
  DELETE_ADDRESS,
  EDIT_ADDRESS,
  EDIT_DATA_URL,
  EMAIL_EXISTS_URL,
  GET_ORDER,
  GET_STORE_DATA,
  GET_USER_DATA,
  LOGIN_URL,
  PASSWORD_CORRECT_URL,
  PRODUCTS_URL,
  REGISTER_FORM_URL,
  REGISTER_URL_WITH_TOKEN,
  RESET_PASSWORD_FORM_URL,
  RESET_PASSWORD_URL_WITH_TOKEN,
  UPDATE_ORDER,
} from "../../global_properties";
import { Approval } from "../../features/register/models/Approval";
import { Product } from "../models/Product";
import { User } from "../models/User";
import { Order } from "../../features/order/model/Order";
import { StoreData } from "../models/StoreData";
import { Address } from "../../features/order/check-out2-step/models/Address";
import { CreditCard } from "../../features/order/check-out2-step/models/CreditCard";
import { LoginForm } from "src/app/features/login/LoginForm";
import { ResetPasswordModel } from "src/app/features/password/password-reset/ResetPasswordModel";
import { RequestResetPassword } from "../../features/password/password-forget/RequestResetPassword";
import { Invoice } from "../../features/order/check-out2-step/models/Invoice";

@Injectable({
  providedIn: "root",
})
export class HttpService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  sendLoginForm(loginForm: LoginForm): Observable<HttpResponse<LoginForm>> {
    return this.httpClient.post<LoginForm>(LOGIN_URL, loginForm, {
      observe: "response",
    });
  }

  sendContactForm(contact: Contact): Observable<Contact> {
    return this.httpClient.post<Contact>(CONTACT_FORM_URL, contact);
  }

  sendRegisterForm(registerForm: RegisterForm): Observable<RegisterForm> {
    return this.httpClient.post<RegisterForm>(REGISTER_FORM_URL, registerForm);
  }

  sendResetPasswordForm(
    requestResetPassword: RequestResetPassword
  ): Observable<RequestResetPassword> {
    return this.httpClient.post<RequestResetPassword>(
      RESET_PASSWORD_FORM_URL,
      requestResetPassword
    );
  }

  checkVerificationToken(verificationToken: string) {
    const parameters = new HttpParams().set("token", verificationToken);
    return this.httpClient.get(REGISTER_URL_WITH_TOKEN, { params: parameters });
  }

  checkResetPasswordToken(
    resetPasswordToken: string,
    resetForm: ResetPasswordModel
  ): Observable<ResetPasswordModel> {
    const parameters = new HttpParams().set("token", resetPasswordToken);
    return this.httpClient.post<ResetPasswordModel>(
      RESET_PASSWORD_URL_WITH_TOKEN,
      resetForm,
      { params: parameters }
    );
  }

  checkEmailIfExist(email: string): Observable<boolean> {
    return this.httpClient.post<boolean>(EMAIL_EXISTS_URL, email);
  }

  checkPassword(oldPassword: string): Observable<boolean> {
    return this.httpClient.post<boolean>(PASSWORD_CORRECT_URL, oldPassword);
  }

  getPersonalData(): Observable<User> {
    return this.httpClient.get<User>(GET_USER_DATA);
  }

  sendPersonalDataUpdate(user: User) {
    return this.httpClient.patch<User>(EDIT_DATA_URL, user);
  }

  sendChangePasswordForm(newPassword: string) {
    return this.httpClient.patch<string>(CHANGE_PASSWORD_FORM_URL, newPassword);
  }

  getApprovals(): Observable<Approval[]> {
    return this.httpClient.get<Approval[]>(APPROVALS_URL);
  }

  getProducts(): Observable<Map<string, Product[]>> {
    return this.httpClient.get<Map<string, Product[]>>(PRODUCTS_URL);
  }

  getProduct(categoryName: string, id: number): Observable<Product> {
    return this.httpClient.get<Product>(PRODUCTS_URL + "/" + id);
  }

  addToOrder(
    productId: number,
    order?: number,
    quantity: number = 1
  ): Observable<Order> {
    let newOrderParameters = new HttpParams();
    newOrderParameters = newOrderParameters
      .set("productId", productId.toString())
      .set("quantity", quantity.toString());
    return this.httpClient.post<Order>(ADD_TO_ORDER_URL, null, {
      params: newOrderParameters,
    });
  }

  redirectToPage(url: string): void {
    this.router.navigateByUrl(url);
  }

  getOrder(): Observable<Order> {
    return this.httpClient.get<Order>(GET_ORDER);
  }

  updateOrder(order: Order): Observable<Order> {
    return this.httpClient.patch<Order>(UPDATE_ORDER, order);
  }

  getStoreData(): Observable<StoreData> {
    return this.httpClient.get<StoreData>(GET_STORE_DATA);
  }

  assignAddressToOrder(address: Address, orderId: string): Observable<Address> {
    const parameters = new HttpParams().set("orderId", orderId);
    return this.httpClient.patch<Address>(ADDRESS, address, {
      params: parameters,
    });
  }

  getAddresses(): Observable<Address[]> {
    return this.httpClient.get<Address[]>(ADDRESSES);
  }

  getAddresses1(): Observable<Address[]> {
    return this.httpClient.get<Address[]>(ADDRESSES1);
  }

  addAddress(address: Address): Observable<Address[]> {
    return this.httpClient.post<Address[]>(ADD_ADDRESS, address);
  }

  addAddress1(address: Address): Observable<Address> {
    return this.httpClient.post<Address>(ADD_ADDRESS1, address);
  }

  editAddress(address: Address): Observable<Address[]> {
    return this.httpClient.patch<Address[]>(EDIT_ADDRESS, address);
  }

  deleteAddress(addressId: number): Observable<Address[]> {
    const parameters = new HttpParams().set("addressId", addressId.toString());
    return this.httpClient.delete<Address[]>(DELETE_ADDRESS, {
      params: parameters,
    });
  }

  validateInvoice(invoice: Invoice): Observable<boolean> {
    return this.httpClient.post<boolean>(
      "http://localhost:7000/invoice",
      invoice
    );
  }

  validatePayment(creditCard: CreditCard): Observable<boolean> {
    return this.httpClient.post<boolean>(
      "http://localhost:7000/credit-card",
      creditCard
    );
  }

  placeOrder(order: Order): Observable<Order> {
    return this.httpClient.post<Order>(
      "http://localhost:7000/order/place-order",
      order
    );
  }

  isOrderDone(orderId: string): Observable<boolean> {
    return this.httpClient.get<boolean>(
      "http://localhost:7000/order/status/" + orderId
    );
  }

  getCurrentOrderProductsQuantity(): Observable<number> {
    return this.httpClient.get<number>(
      "http://localhost:7000/order/products/quantity"
    );
  }
}
