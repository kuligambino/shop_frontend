import { Injectable } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class QuantityUpdateService {
  private readonly updateQuantity: Subject<void> = new Subject();

  triggerQuantity(): void {
    this.updateQuantity.next();
  }

  getUpdateQuantity(): Observable<void> {
    return this.updateQuantity.asObservable();
  }
}
