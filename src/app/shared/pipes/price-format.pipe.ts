import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "priceFormat",
})
export class PriceFormatPipe implements PipeTransform {
  transform(value: number): string {
    return this.replaceDotsIntoComma(this.roundTo2DecimalPlaces(value)) + " zł";
  }

  private replaceDotsIntoComma(numberAsString: string): string {
    return numberAsString.replace(".", ",");
  }

  private roundTo2DecimalPlaces(value: number): string {
    return (Math.round(value * 100) / 100).toFixed(2);
  }
}
