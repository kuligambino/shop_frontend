import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "creditCard",
})
export class CreditCardPipe implements PipeTransform {
  transform(value: string): unknown {
    return value.substr(-4);
  }
}
