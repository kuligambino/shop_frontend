import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "whiteLines",
})
export class WhiteLinesPipe implements PipeTransform {
  transform(value: any) {
    if (!value) {
      return "";
    }
    return value.replace(/\s*/g, "");
  }
}
