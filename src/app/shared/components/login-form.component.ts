import { Component, OnInit } from "@angular/core";
import {
  BAD_CREDENTIALS,
  EMAIL_FORMAT_VALIDATION,
  FIELD_VALIDATION,
  FORGET_PASSWORD_BUTTON,
  HOME_PAGE_URL,
  LOGIN_BUTTON,
  REGISTER_BUTTON,
} from "../../global_properties";
import { FormGroup } from "@angular/forms";
import { LoginForm } from "../../features/login/LoginForm";
import { ApiError } from "../../core/exceptions/ApiError";
import { mapFormToModel } from "../helpers/login-helper";
import { HttpService } from "../services/http.service";
import { AuthService } from "../services/auth.service";
import { FormService } from "../services/form.service";
import { DONT_HAVE_ACCOUNT, LOGIN_TITLE } from "../constants";

@Component({
  selector: "app-login-form",
  templateUrl: "./login-form.component.html",
  styleUrls: ["./login-form.component.scss"],
})
export class LoginFormComponent implements OnInit {
  fieldValidation = FIELD_VALIDATION;
  emailValidation = EMAIL_FORMAT_VALIDATION;
  title = LOGIN_TITLE;
  loginButton = LOGIN_BUTTON;
  dontHaveAccount = DONT_HAVE_ACCOUNT;
  registerButton = REGISTER_BUTTON;
  forgetPassword = FORGET_PASSWORD_BUTTON;

  badCredentials = "";
  loginFormGroup: FormGroup;
  loginForm: LoginForm;
  errors: ApiError[] = [];

  constructor(
    private httpService: HttpService,
    private authService: AuthService,
    private formService: FormService
  ) {}

  ngOnInit(): void {
    this.loginFormGroup = this.formService.initLoginForm();
  }

  submit(): void {
    this.loginForm = mapFormToModel(this.loginFormGroup);
    this.httpService.sendLoginForm(this.loginForm).subscribe(
      (resp) => {
        this.authService.saveTokenToCookies(resp);
        this.authService.saveOrderToCookies(resp);
        this.httpService.redirectToPage(HOME_PAGE_URL);
      },
      () => (this.badCredentials = BAD_CREDENTIALS)
    );
    this.loginFormGroup.reset();
  }
}
