import { Directive } from "@angular/core";
import { AbstractControl, NG_VALIDATORS, Validator } from "@angular/forms";

@Directive({
  selector: "[appAcceptedApprovals]",
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: AcceptedApprovalsDirective,
      multi: true,
    },
  ],
})
export class AcceptedApprovalsDirective implements Validator {
  validate(control: AbstractControl): { [key: string]: any } {
    const userApprovals = control.value as Array<string>;
    const requiredApprovals = ["REGULATIONS", "PERSONAL_DATA_PROCESSING"];
    let result = true;
    for (const app of requiredApprovals) {
      if (!userApprovals.includes(app)) {
        result = false;
      }
    }
    return !result ? { approvalsNotAccepted: true } : null;
  }
}
