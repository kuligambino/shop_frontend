import { HttpService } from "../services/http.service";
import {
  AbstractControl,
  AsyncValidatorFn,
  ValidationErrors,
} from "@angular/forms";
import { Observable } from "rxjs/internal/Observable";
import { map } from "rxjs/operators";

export class EmailExistsValidator {
  static emailExists(httpService: HttpService): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors> => {
      return httpService
        .checkEmailIfExist(control.value)
        .pipe(
          map((emailExists) =>
            emailExists ? null : { emailDoesntExists: true }
          )
        );
    };
  }
}
