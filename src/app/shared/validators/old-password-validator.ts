import { HttpService } from "../services/http.service";
import {
  AbstractControl,
  AsyncValidatorFn,
  ValidationErrors,
} from "@angular/forms";
import { Observable } from "rxjs/internal/Observable";
import { map } from "rxjs/operators";

export class OldPasswordValidator {
  static isOldPasswordSame(httpService: HttpService): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors> => {
      return httpService.checkPassword(control.value).pipe(
        map((isSame: boolean) => {
          return isSame ? null : { oldPasswordIsDifferent: true };
        })
      );
    };
  }
}
