import { Directive, Input } from "@angular/core";
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from "@angular/forms";
import { Subscription } from "rxjs";

@Directive({
  selector: "[appConfirmPassword]",
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ConfirmPasswordDirective,
      multi: true,
    },
  ],
})
export class ConfirmPasswordDirective implements Validator {
  @Input("appConfirmPassword") controlNameToCompare: string;

  validate(c: AbstractControl): ValidationErrors | null {
    const controlToCompare = c.parent.get(this.controlNameToCompare);

    if (controlToCompare) {
      const subscription: Subscription =
        controlToCompare.valueChanges.subscribe(() => {
          c.updateValueAndValidity();
          subscription.unsubscribe();
        });
    }
    return controlToCompare && controlToCompare.value !== c.value
      ? { appConfirmPassword: true }
      : null;
  }
}
