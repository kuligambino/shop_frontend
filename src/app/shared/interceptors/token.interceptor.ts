import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { AuthService } from "../services/auth.service";
import { catchError } from "rxjs/operators";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const jWToken = this.authService.getJwToken();
    const orderId = this.authService.getOrderId();
    request = this.addOrderId(request, orderId);
    if (jWToken) {
      request = this.addToken(request, jWToken);
      return next.handle(request);
    }
    return next.handle(request).pipe(
      catchError((error) => {
        if (error instanceof HttpErrorResponse && error.status === 401) {
          return throwError(error);
        } else {
          return next.handle(request);
        }
      })
    );
  }

  private addToken(request: HttpRequest<any>, jWToken: string) {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${jWToken}`,
      },
    });
  }

  private addOrderId(request: HttpRequest<any>, orderId: string) {
    return request.clone({
      setHeaders: {
        order: orderId,
      },
    });
  }
}
