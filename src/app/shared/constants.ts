export const LOGIN_TITLE = "Zaloguj się";
export const DONT_HAVE_ACCOUNT = "Nie masz konta? ";
export const TOKEN_PREFIX = "Bearer ";
export const REPLACE_VALUE = "";
export const HEADER_STRING = "Authorization";
export const JWT_TOKEN = "JWToken";
