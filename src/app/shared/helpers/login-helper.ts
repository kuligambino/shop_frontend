import { FormGroup } from "@angular/forms";
import { LoginForm } from "../../features/login/LoginForm";

export function mapFormToModel(loginFormGroup: FormGroup): LoginForm {
  return {
    email: loginFormGroup.value.email,
    password: loginFormGroup.value.password,
  };
}
