import { HttpErrorResponse } from "@angular/common/http";
import { ApiError } from "../../core/exceptions/ApiError";

export function mapHttpErrors(httpError: HttpErrorResponse) {
  return httpError.error.map((error: ApiError) => ({
    field: error.field,
    code: error.code,
    message: error.message,
  }));
}
