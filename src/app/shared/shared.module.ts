import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {
  RecaptchaModule,
  RecaptchaFormsModule,
  RecaptchaSettings,
  RECAPTCHA_SETTINGS,
} from "ng-recaptcha";
import { MatDialogModule } from "@angular/material/dialog";

import { AcceptedApprovalsDirective } from "./validators/accepted-approvals.directive";
import { BlockCopyPasteDirective } from "./directives/block-copy-paste.directive";
import { ConfirmPasswordDirective } from "./validators/confirm-password.directive";
import { CreditCardInputDirective } from "./directives/credit-card-input.directive";
import { PhoneNumberInputDirective } from "./directives/phone-number-input.directive";
import { PostCodeInputDirective } from "./directives/post-code-input.directive";
import { CreditCardPipe } from "./pipes/credit-card.pipe";
import { DateFormatPipe } from "./pipes/date-format.pipe";
import { PriceFormatPipe } from "./pipes/price-format.pipe";
import { PasswordStrengthComponent } from "./components/password-strength.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { WhiteLinesPipe } from "./pipes/white-lines.pipe";
import { LoginFormComponent } from "./components/login-form.component";
import { RouterModule } from "@angular/router";
import { WeightPipe } from "./pipes/weight.pipe";

@NgModule({
  declarations: [
    AcceptedApprovalsDirective,
    BlockCopyPasteDirective,
    ConfirmPasswordDirective,
    CreditCardInputDirective,
    PhoneNumberInputDirective,
    PostCodeInputDirective,
    CreditCardPipe,
    DateFormatPipe,
    PriceFormatPipe,
    PasswordStrengthComponent,
    WhiteLinesPipe,
    LoginFormComponent,
    WeightPipe,
  ],
  exports: [
    AcceptedApprovalsDirective,
    BlockCopyPasteDirective,
    ConfirmPasswordDirective,
    CreditCardInputDirective,
    PhoneNumberInputDirective,
    PostCodeInputDirective,
    CreditCardPipe,
    DateFormatPipe,
    PriceFormatPipe,
    PasswordStrengthComponent,
    MatCheckboxModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    FormsModule,
    ReactiveFormsModule,
    WhiteLinesPipe,
    LoginFormComponent,
    WeightPipe,
  ],
  imports: [
    CommonModule,
    MatCheckboxModule,
    MatDialogModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  providers: [
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {
        siteKey: "6Lf9EO8UAAAAAEM7DSCU_UFfGV-FzegjENyV1A3g",
      } as RecaptchaSettings,
    },
  ],
})
export class SharedModule {}
