import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";
import { AuthService } from "../services/auth.service";
import { HttpService } from "../services/http.service";
import { MY_ACCOUNT_URL } from "../../global_properties";

@Injectable({
  providedIn: "root",
})
export class NotLoggedGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private httpService: HttpService
  ) {}

  canActivate() {
    if (this.authService.isAuthenticated()) {
      this.httpService.redirectToPage(MY_ACCOUNT_URL);
    } else {
      return !this.authService.isAuthenticated();
    }
  }
}
