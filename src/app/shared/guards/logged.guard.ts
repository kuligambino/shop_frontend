import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";
import { AuthService } from "../services/auth.service";
import { HttpService } from "../services/http.service";
import { REDIRECT_TO_LOGIN } from "../../global_properties";

@Injectable({
  providedIn: "root",
})
export class LoggedGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private httpService: HttpService
  ) {}

  canActivate() {
    if (!this.authService.isAuthenticated()) {
      this.httpService.redirectToPage(REDIRECT_TO_LOGIN);
    } else {
      return this.authService.isAuthenticated();
    }
  }
}
