import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { HttpService } from "../services/http.service";
import { map, switchMap } from "rxjs/operators";
import { DataService } from "../services/data.service";
import { Order } from "../../features/order/model/Order";

@Injectable({
  providedIn: "root",
})
export class FinishOrderCanActivateGuard implements CanActivate {
  constructor(
    private httpService: HttpService,
    private dataService: DataService,
    private router: Router
  ) {}

  canActivate() {
    return this.dataService.currentOrder.pipe(
      switchMap((order: Order) => {
        return this.httpService.isOrderDone(order.id.toString()).pipe(
          map((isDone: boolean) => {
            return isDone ? true : this.router.parseUrl("order");
          })
        );
      })
    );
  }
}
