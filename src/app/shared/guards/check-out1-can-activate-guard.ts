import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { AuthService } from "../services/auth.service";
import { HttpService } from "../services/http.service";
import { CHECK_OUT_2_STEP } from "../../global_properties";
import { map } from "rxjs/operators";
import { Order } from "../../features/order/model/Order";

@Injectable({
  providedIn: "root",
})
export class CheckOut1CanActivateGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private httpService: HttpService,
    private router: Router
  ) {}

  canActivate() {
    if (this.authService.isAuthenticated()) {
      this.httpService.redirectToPage(CHECK_OUT_2_STEP);
    } else {
      return this.httpService.getOrder().pipe(
        map((order: Order) => {
          return order.orderProducts.length !== 0 &&
            !this.authService.isAuthenticated()
            ? true
            : this.router.parseUrl("order");
        })
      );
    }
  }
}
