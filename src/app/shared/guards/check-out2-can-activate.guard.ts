import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { map } from "rxjs/operators";
import { Order } from "../../features/order/model/Order";
import { OrderService } from "../services/order.service";
import { HttpService } from "../services/http.service";

@Injectable({
  providedIn: "root",
})
export class CheckOut2CanActivateGuard implements CanActivate {
  constructor(
    private orderService: OrderService,
    private httpService: HttpService,
    private router: Router
  ) {}

  canActivate() {
    return this.httpService.getOrder().pipe(
      map((order: Order) => {
        return order.orderProducts.length !== 0
          ? true
          : this.router.parseUrl("order");
      })
    );
  }
}
