// HTTP SERVICE LINKS

export const LOCALHOST = "http://localhost:7000";
export const CONTACT_FORM_URL = `${LOCALHOST}/contact`;
export const REGISTER_FORM_URL = `${LOCALHOST}/register`;
export const REGISTER_URL_WITH_TOKEN = `${LOCALHOST}/register/confirm`;
export const LOGIN_URL = `${LOCALHOST}/login`;
export const EMAIL_EXISTS_URL = `${LOCALHOST}/register/email-exists`;
export const RESET_PASSWORD_URL_WITH_TOKEN = `${LOCALHOST}/password/reset/confirm`;
export const RESET_PASSWORD_FORM_URL = `${LOCALHOST}/password/forget`;
export const CHANGE_PASSWORD_FORM_URL = `${LOCALHOST}/change-password`;
export const EDIT_DATA_URL = `${LOCALHOST}/edit-user`;
export const GET_USER_DATA = `${LOCALHOST}/user-data`;
export const PASSWORD_CORRECT_URL = `${LOCALHOST}/control-password`;
export const APPROVALS_URL = `${LOCALHOST}/approvals`;
export const PRODUCTS_URL = `${LOCALHOST}/products`;
export const ADD_TO_ORDER_URL = `${LOCALHOST}/order/add`;
export const GET_ORDER = `${LOCALHOST}/order`;
export const UPDATE_ORDER = `${LOCALHOST}/order/update`;
export const GET_STORE_DATA = `${LOCALHOST}/store-data`;
export const ADDRESS = `${LOCALHOST}/address`;
export const ADDRESSES = `${LOCALHOST}/addresses`;
export const ADDRESSES1 = `${LOCALHOST}/user-addresses`;
export const ADD_ADDRESS = `${LOCALHOST}/add-address`;
export const ADD_ADDRESS1 = `${LOCALHOST}/add-user-address`;
export const EDIT_ADDRESS = `${LOCALHOST}/edit-address`;
export const DELETE_ADDRESS = `${LOCALHOST}/delete-address`;
export const REGISTER_SUCCEED_URL = "/register/succeed";
export const RESET_PASSWORD_SUCCEED_URL = "/password/forget/succeed";
export const REGISTER_CONFIRM_SUCCEED_URL = "/register/succeed";
export const HOME_PAGE_URL = "/home";
export const CHECK_OUT_2_STEP = "order/checkout-2";
export const REDIRECT_TO_LOGIN = "/login";
export const MY_ACCOUNT_URL = "/my-account";
export const RESET_PASSWORD_CONFIRM_SUCCESS_URL = "/password/reset/succeed";
export const PAGE_INACTIVE_URL = "/page-inactive";

// VALIDATION MESSAGES

export const EMAIL_FORMAT_VALIDATION = "Zły format maila!";
export const FIELD_VALIDATION = "Pole jest wymagane!";
export const PASSWORD_VALIDATION = "Hasła muszą być takie same!";
export const PATTERN_VALIDATION =
  "Hasło musi mieć co najmniej 8 znaków w tym jedną duża literę i znak specjalny!";
export const BAD_CREDENTIALS = "Niepoprawny login lub hasło";
export const EMAIL_EXISTS_VALIDATION =
  "Użytkownik o takim emailu już istnieję!";
export const EMAIL_NOT_EXISTS_VALIDATION =
  "Uzytkownik o takim emailu nie istnieję!";
export const PHONE_FORMAT_VALIDATION =
  "Zły format numeru! Przykładowy zapis: 123-456-789.";
export const POST_CODE_FORMAT_VALIDATION =
  "Zły format kodu pocztowego! Przykładowy zapis: 12-123.";
export const CREDIT_CARD_PATTERN_VALIDATION =
  "Nieprawidłowy numer karty kredytowej!";
export const CVV_PATTERN_VALIDATION = "Nieprawidłowy numer CVV!";
export const NIP_PATTERN_VALIDATION = "Nieprawidłowy numer NIP!";
export const WRONGLY_FILLED_FORM = "Źle wypełniony formularz";
export const PAYMENT_ERROR = "Problem z płatnością!";

// BUTTONS TEXTS

export const SEND_BUTTON = "Wyślij";
export const ACCEPT_BUTTON = "Akceptuj";
export const LOGIN_BUTTON = "Zaloguj się";
export const REGISTER_BUTTON = "Zarejestruj się";
export const FORGET_PASSWORD_BUTTON = "Nie pamiętasz hasła?";
export const RESET_FORM_BUTTON = "Resetuj formularz";
export const CREATE_ACCOUNT_BUTTON = "Stwórz konto";
export const CONFIRM_REGISTER_BUTTON = "Potwierdź rejestrację";
export const ADD_TO_ORDER = "Dodaj do koszyka";
export const COOKIE_LAW_NAME = "cookieLawSeen";
export const HOME = "Strona główna";
export const LOGIN = "Zaloguj się ";

// REGISTER COMPONENT

export const REGISTER_DESCRIPTION = [
  "Jeden globalny login, który daje Ci dostęp do produktów i usług SupplyShop.",
  "Szybsze finalizowanie transakcji, zachowanie Twoich danych i szczegółów płatności.",
  "Zachowaj swój adres i szczegóły płatności, by szybciej finalizować zakupy.",
  "Dostęp do historii zamówień",
  "Monitorowanie zamówienia.",
  "Tworzenie listy życzeń, by wiedzieć, co dzieje się z ulubionymi produktami.",
];
export const REGISTER_TITLE = "Utwórz konto";
export const REGISTER_MESSAGE = "TWÓJ GLOBALNY LOGIN DAJE CI DOSTĘP DO: ";

// FORGET-PASSWORD COMPONENT

export const FORGET_PASSWORD_HEADER = "NIE PAMIĘTASZ HASŁA?";
export const FORGET_PASSWORD_DESCRIPTION =
  "Sprawdź swoją skrzynkę mailową i postępuj zgodnie z linkiem, aby zresetować swoje hasło.";
export const FORGET_PASSWORD_TITLE =
  "Wysłano link do zmiany hasła na skrzynkę ";
