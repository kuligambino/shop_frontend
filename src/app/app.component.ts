import { Component } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { COOKIE_LAW_NAME } from "./global_properties";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  constructor(private cookieService: CookieService) {}

  acceptedCookiePolicy() {
    return this.cookieService.get(COOKIE_LAW_NAME);
  }
}
