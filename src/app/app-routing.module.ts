import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoggedGuard } from "./shared/guards/logged.guard";
import { NotLoggedGuard } from "./shared/guards/not-logged.guard";
import { NotFoundComponent } from "./core/not-found/not-found.component";
import { ProductComponent } from "./features/product/product.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full",
  },
  {
    path: "home",
    loadChildren: () =>
      import("./features/home/home.module").then((m) => m.HomeModule),
  },
  {
    path: "register",
    loadChildren: () =>
      import("./features/register/register.module").then(
        (m) => m.RegisterModule
      ),
  },
  {
    path: "password",
    loadChildren: () =>
      import("./features/password/password.module").then(
        (m) => m.PasswordModule
      ),
  },
  {
    path: "my-account",
    canActivate: [LoggedGuard],
    loadChildren: () =>
      import("./features/my-account/my-account.module").then(
        (m) => m.MyAccountModule
      ),
  },
  {
    path: "order",
    loadChildren: () =>
      import("./features/order/order.module").then((m) => m.OrderModule),
  },
  {
    path: "about",
    loadChildren: () =>
      import("./features/about-us/about-us.module").then(
        (m) => m.AboutUsModule
      ),
  },
  {
    path: "contact",
    loadChildren: () =>
      import("./features/contact/contact.module").then((m) => m.ContactModule),
  },
  {
    path: "login",
    canActivate: [NotLoggedGuard],
    loadChildren: () =>
      import("./features/login/login.module").then((m) => m.LoginModule),
  },
  {
    path: "products/:categoryName/:id",
    component: ProductComponent,
  },
  {
    path: "**",
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: "legacy" })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
