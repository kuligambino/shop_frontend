import { Component } from "@angular/core";
import {
  REGISTER_DESCRIPTION,
  REGISTER_MESSAGE,
  REGISTER_TITLE,
} from "src/app/global_properties";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent {
  registerDescription = REGISTER_DESCRIPTION;
  registerTitle = REGISTER_TITLE;
  registerMessage = REGISTER_MESSAGE;
}
