import { Component, OnInit } from "@angular/core";
import { updateUser } from "./profile-helper";
import { FIELD_VALIDATION } from "../../../global_properties";
import { EDIT_DATA_BUTTON, TITLE } from "../constants";
import { FormGroup } from "@angular/forms";
import { User } from "../../../shared/models/User";
import { ApiError } from "../../../core/exceptions/ApiError";
import { HttpService } from "../../../shared/services/http.service";
import { FormService } from "../../../shared/services/form.service";

@Component({
  selector: "app-change-personal-data-form",
  templateUrl: "./change-personal-data-form.component.html",
  styleUrls: ["./change-personal-data-form.component.scss"],
})
export class ChangePersonalDataFormComponent implements OnInit {
  fieldValidation = FIELD_VALIDATION;
  title = TITLE;
  editDataButton = EDIT_DATA_BUTTON;

  personalDataFormGroup: FormGroup;
  user: User;
  errors: ApiError[] = [];
  successMessage = "";

  constructor(
    private httpService: HttpService,
    private formService: FormService
  ) {}

  ngOnInit(): void {
    this.httpService.getPersonalData().subscribe((user: User) => {
      this.user = user;
      this.personalDataFormGroup = this.formService.initPersonalDataForm(
        this.user
      );
    });
  }

  editPersonalData(): void {
    this.user = updateUser(this.personalDataFormGroup, this.user);
    this.httpService.sendPersonalDataUpdate(this.user).subscribe(() => {
      this.successMessage = "Udało się zmienić danę!";
      this.resetMessage();
    });
  }

  private resetMessage(): void {
    setTimeout(() => {
      this.successMessage = "";
    }, 3000);
  }
}
