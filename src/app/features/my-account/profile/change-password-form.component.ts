import { Component, OnInit } from "@angular/core";
import {
  FIELD_VALIDATION,
  PASSWORD_VALIDATION,
  PATTERN_VALIDATION,
} from "../../../global_properties";
import { CHANGE_PASSWORD_BUTTON, OLD_PASSWORD_VALIDATION } from "../constants";
import { FormGroup } from "@angular/forms";
import { ChangePasswordForm } from "./ChangePasswordForm";
import { ApiError } from "../../../core/exceptions/ApiError";
import { HttpService } from "../../../shared/services/http.service";
import { FormService } from "../../../shared/services/form.service";
import { mapChangePasswordFormToModel } from "./profile-helper";

@Component({
  selector: "app-change-password-form",
  templateUrl: "./change-password-form.component.html",
  styleUrls: ["./change-password-form.component.scss"],
})
export class ChangePasswordFormComponent implements OnInit {
  fieldValidation = FIELD_VALIDATION;
  patternValidation = PATTERN_VALIDATION;
  passwordValidation = PASSWORD_VALIDATION;
  changePasswordButton = CHANGE_PASSWORD_BUTTON;
  oldPasswordValidation = OLD_PASSWORD_VALIDATION;

  changePasswordFormGroup: FormGroup;
  changePasswordForm = new ChangePasswordForm();
  errors: ApiError[] = [];
  passwordIsValid = false;
  changedPasswordMessage = "";

  constructor(
    private httpService: HttpService,
    private formService: FormService
  ) {}

  ngOnInit(): void {
    this.changePasswordFormGroup = this.formService.initResetPasswordForm();
  }

  changePassword(): void {
    this.changePasswordForm = mapChangePasswordFormToModel(
      this.changePasswordFormGroup
    );
    this.httpService
      .sendChangePasswordForm(this.changePasswordForm.newPassword)
      .subscribe(() => {
        this.changedPasswordMessage = "Udało się zmienić hasło";
        this.resetForm();
      });
  }

  passwordValid(event): void {
    this.passwordIsValid = event;
  }

  private resetForm(): void {
    this.changePasswordFormGroup.reset();
    setTimeout(() => {
      this.changedPasswordMessage = "";
    }, 3000);
  }
}
