export class ChangePasswordForm {
  constructor(
    public oldPassword?: string,
    public newPassword?: string,
    public repeatedNewPassword?: string
  ) {}
}
