import { FormGroup } from "@angular/forms";
import { User } from "../../../shared/models/User";
import { ChangePasswordForm } from "./ChangePasswordForm";

export function updateUser(personalDataFormGroup: FormGroup, user: User): User {
  user.firstName = personalDataFormGroup.value.firstName;
  user.lastName = personalDataFormGroup.value.lastName;
  user.phoneNumber = personalDataFormGroup.value.phoneNumber;
  user.birthDate = personalDataFormGroup.value.birthDate;
  return user;
}

export function mapChangePasswordFormToModel(
  changePasswordFormGroup: FormGroup
): ChangePasswordForm {
  return {
    oldPassword: changePasswordFormGroup.value.oldPassword,
    newPassword: changePasswordFormGroup.value.newPassword,
    repeatedNewPassword: changePasswordFormGroup.value.repeatedNewPassword,
  };
}
