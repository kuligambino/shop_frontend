import { Component } from "@angular/core";
import { AuthService } from "../../shared/services/auth.service";
import { ADDRESSES, ORDERS, PROFILE, LOGOUT } from "./constants";

@Component({
  selector: "app-my-account",
  templateUrl: "./my-account.component.html",
  styleUrls: ["./my-account.component.scss"],
})
export class MyAccountComponent {
  profile = PROFILE;
  addresses = ADDRESSES;
  orders = ORDERS;
  logOut = LOGOUT;

  constructor(private authService: AuthService) {}

  logoutUser() {
    this.authService.logout();
  }
}
