import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MyAccountRoutingModule } from "./my-account-routing.module";
import { MyAddressesComponent } from "./my-addresses/my-addresses.component";
import { MyAccountComponent } from "./my-account.component";
import { ProfileComponent } from "./profile/profile.component";
import { SharedModule } from "src/app/shared/shared.module";
import { ChangePasswordFormComponent } from "./profile/change-password-form.component";
import { ChangePersonalDataFormComponent } from "./profile/change-personal-data-form.component";
import { SingleAddressFormComponent } from "./my-addresses/single-address-form.component";
import { DynamicFormDirective } from "./my-addresses/dynamic-form.directive";

@NgModule({
  declarations: [
    MyAddressesComponent,
    MyAccountComponent,
    ProfileComponent,
    ChangePasswordFormComponent,
    ChangePersonalDataFormComponent,
    SingleAddressFormComponent,
    DynamicFormDirective,
  ],
  imports: [CommonModule, MyAccountRoutingModule, SharedModule],
  exports: [MyAddressesComponent, MyAccountComponent, ProfileComponent],
})
export class MyAccountModule {}
