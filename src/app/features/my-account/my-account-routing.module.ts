import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MyAccountComponent } from "./my-account.component";
import { MyAddressesComponent } from "./my-addresses/my-addresses.component";
import { ProfileComponent } from "./profile/profile.component";
import { AddressesResolver } from "./my-addresses/addresses.resolver";

const routes: Routes = [
  {
    path: "",
    component: MyAccountComponent,
  },
  {
    path: "addresses",
    component: MyAddressesComponent,
    resolve: { addresses: AddressesResolver },
  },
  {
    path: "profile",
    component: ProfileComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyAccountRoutingModule {}
