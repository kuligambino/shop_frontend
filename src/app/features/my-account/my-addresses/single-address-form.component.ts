import { Component, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";
import {
  FIELD_VALIDATION,
  POST_CODE_FORMAT_VALIDATION,
} from "../../../global_properties";
import { ApiError } from "../../../core/exceptions/ApiError";

@Component({
  selector: "app-single-address-form",
  templateUrl: "./single-address-form.component.html",
  styleUrls: ["./single-address-form.component.scss"],
})
export class SingleAddressFormComponent {
  @Input() userAddress: FormGroup;
  fieldValidation = FIELD_VALIDATION;
  postCodeValidationMessage = POST_CODE_FORMAT_VALIDATION;
  errors: ApiError[] = [];
  isSubmitted = false;
}
