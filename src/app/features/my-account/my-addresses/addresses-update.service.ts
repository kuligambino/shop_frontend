import { Injectable } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class AddressesUpdateService {
  private readonly updateAddresses: Subject<void> = new Subject();
  private readonly delete: Subject<void> = new Subject();
  private readonly save: Subject<void> = new Subject();
  private readonly edit: Subject<void> = new Subject();

  triggerUpdateAddresses(): void {
    this.updateAddresses.next();
  }

  getUpdateAddresses(): Observable<void> {
    return this.updateAddresses.asObservable();
  }

  triggerDelete(): void {
    this.delete.next();
  }

  getDeleteUpdate(): Observable<void> {
    return this.delete.asObservable();
  }

  triggerSave(): void {
    this.save.next();
  }

  getSaveUpdate(): Observable<void> {
    return this.save.asObservable();
  }

  triggerEdit(): void {
    this.edit.next();
  }

  getEditUpdate(): Observable<void> {
    return this.edit.asObservable();
  }
}
