import { Component, ComponentFactoryResolver, OnInit } from "@angular/core";
import { HttpService } from "../../../shared/services/http.service";
import { ActivatedRoute } from "@angular/router";
import { FormArray, FormGroup } from "@angular/forms";
import { FormService } from "../../../shared/services/form.service";
import { Address } from "./Address";

@Component({
  selector: "app-my-addresses",
  templateUrl: "./my-addresses.component.html",
  styleUrls: ["./my-addresses.component.scss"],
})
export class MyAddressesComponent implements OnInit {
  addressesForm: FormGroup;
  isSaved = false;
  currentIndex = -1;
  isEditableForm = false;

  constructor(
    private httpService: HttpService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private activatedRoute: ActivatedRoute,
    private formService: FormService
  ) {}

  ngOnInit(): void {
    this.addressesForm = this.formService.initAddressesForm();
    this.activatedRoute.data.subscribe((data) => {
      this.addAllAddressesToForm(data.addresses);
    });
  }

  get addresses(): FormArray {
    return <FormArray>this.addressesForm.get("addresses");
  }

  addAddress() {
    this.addresses.push(this.buildForm());
    this.currentIndex++;
    this.isEditableForm = true;
  }

  deleteAddress(index: number) {
    const address = <Address>this.addresses.controls[index].value;
    this.addresses.removeAt(index);
    if (address.id) {
      this.httpService.deleteAddress(address.id).subscribe();
    }
    this.isEditableForm = false;
  }

  saveAddress(index: number) {
    this.httpService
      .addAddress1(this.addresses.at(index).value)
      .subscribe((savedAddress: Address) => {
        this.addresses.setControl(index, this.buildForm(savedAddress));
        this.isEditableForm = false;
        this.addresses.at(index).disable();
      });
  }

  editAddress(index: number) {
    this.addresses.at(index).enable();
    this.isEditableForm = true;
  }

  private addAllAddressesToForm(addresses: Address[]) {
    for (let address of addresses) {
      this.addresses.push(this.buildForm(address));
      this.addresses.controls.forEach((fg) => fg.disable());
    }
  }

  private buildForm(address?: Address): FormGroup {
    return this.formService.initAddressForm(address);
  }
}
