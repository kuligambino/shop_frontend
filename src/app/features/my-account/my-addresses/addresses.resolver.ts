import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { Address } from "./Address";
import { HttpService } from "../../../shared/services/http.service";

@Injectable({
  providedIn: "root",
})
export class AddressesResolver implements Resolve<Address[]> {
  constructor(private httpService: HttpService) {}

  resolve(): Observable<Address[]> {
    return this.httpService.getAddresses1();
  }
}
