import { Directive, ViewContainerRef } from "@angular/core";

@Directive({
  selector: "[appDynamicForm]",
})
export class DynamicFormDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
