export const PROFILE = "DANE OSOBOWE";
export const ADDRESSES = "MOJE ADRESY";
export const ORDERS = "ZAMOWIENIA";
export const LOGOUT = "WYLOGUJ SIĘ";
export const TITLE = "MOJE DANE";
export const CHANGE_PASSWORD_BUTTON = "Zmień hasło";
export const EDIT_DATA_BUTTON = "Edytuj dane";
export const OLD_PASSWORD_VALIDATION = "Hasła muszą być takie same!";
