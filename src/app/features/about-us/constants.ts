export const MESSAGE_ABOUT_US =
  "Jesteśmy polską firmą oferująca suplementy diety i odżywki dla sportowców, które wykonane zostały ze starannie\n" +
  "      wyselekcjonowanych,\n" +
  "      bezpiecznych dla zdrowia składników. Wśród produktów tej marki znajdują się odżywki proteinowe, suplementy\n" +
  "      aminokwasowe,\n" +
  "      preparaty przedtreningowe i kreatynowe. Firma umożliwia również zakup środków wywołujących silną pompę mięśniową\n" +
  "      poprzez znaczne\n" +
  "      podniesienie poziomu tlenku azotu we krwi oraz preparatów przywracających równowagę kwasową zasadową. Na uwagę\n" +
  "      zasługują także\n" +
  "      suplementy ułatwiające redukcję tkanki tłuszczowej, regulujące metabolizm oraz wpływające na poprawę stanu włosów,\n" +
  "      skóry i paznokci.";
export const TITLE_ABOUT_US = "Sport to zdrowie!";
export const COMPANY_NAME = "KKWJ";
