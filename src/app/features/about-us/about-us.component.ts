import { Component } from "@angular/core";
import {
  COMPANY_NAME,
  MESSAGE_ABOUT_US,
  TITLE_ABOUT_US,
} from "src/app/features/about-us/constants";

@Component({
  selector: "app-about-us",
  templateUrl: "./about-us.component.html",
  styleUrls: ["./about-us.component.scss"],
})
export class AboutUsComponent {
  companyName = COMPANY_NAME;
  mainMessage = MESSAGE_ABOUT_US;
  title = TITLE_ABOUT_US;
}
