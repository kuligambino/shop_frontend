import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { RegisterRoutingModule } from "./register-routing.module";
import { RegisterSuccessComponent } from "./register-success/register-success.component";
import { RegisterPageComponent } from "./register-page/register-page.component";
import { RegisterComponent } from "./register.component";
import { SharedModule } from "src/app/shared/shared.module";
import { RegisterConfirmContainerComponent } from "./register-confirmation/register-confirm-container.component";
import { RegisterConfirmComponent } from "./register-confirmation/register-confirm/register-confirm.component";
import { RegisterConfirmSuccessComponent } from "./register-confirmation/register-confirm-success/register-confirm-success.component";

@NgModule({
  declarations: [
    RegisterComponent,
    RegisterPageComponent,
    RegisterSuccessComponent,
    RegisterConfirmComponent,
    RegisterConfirmSuccessComponent,
    RegisterConfirmContainerComponent,
  ],
  imports: [CommonModule, SharedModule, RegisterRoutingModule],
})
export class RegisterModule {}
