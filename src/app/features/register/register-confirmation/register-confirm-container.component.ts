import { Component } from "@angular/core";

@Component({
  selector: "app-register-confirm-container",
  templateUrl: "./register-confirm-container.component.html",
  styleUrls: ["./register-confirm-container.component.scss"],
})
export class RegisterConfirmContainerComponent {}
