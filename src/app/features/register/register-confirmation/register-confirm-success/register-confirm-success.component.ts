import { Component } from "@angular/core";
import { HOME, LOGIN } from "../../../../global_properties";
import { CONFIRMED_REGISTER } from "src/app/features/register/constants";

@Component({
  selector: "app-register-confirm-success",
  templateUrl: "./register-confirm-success.component.html",
  styleUrls: ["./register-confirm-success.component.scss"],
})
export class RegisterConfirmSuccessComponent {
  confirmedRegister = CONFIRMED_REGISTER;
  login = LOGIN;
  home = HOME;
}
