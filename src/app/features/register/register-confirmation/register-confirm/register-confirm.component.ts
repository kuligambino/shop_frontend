import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { CONFIRM_REGISTER } from "src/app/features/register/constants";
import { HttpService } from "src/app/shared/services/http.service";
import {
  CONFIRM_REGISTER_BUTTON,
  PAGE_INACTIVE_URL,
  REGISTER_CONFIRM_SUCCEED_URL,
} from "../../../../global_properties";

@Component({
  selector: "app-register-confirm",
  templateUrl: "./register-confirm.component.html",
  styleUrls: ["./register-confirm.component.scss"],
})
export class RegisterConfirmComponent implements OnInit {
  confirmRegister = CONFIRM_REGISTER;
  confirmRegisterButton = CONFIRM_REGISTER_BUTTON;

  verificationToken: string;

  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.verificationToken = params["token"];
    });
  }

  checkVerificationToken() {
    this.httpService.checkVerificationToken(this.verificationToken).subscribe(
      () => {
        this.httpService.redirectToPage(REGISTER_CONFIRM_SUCCEED_URL);
      },
      () => {
        this.httpService.redirectToPage(PAGE_INACTIVE_URL);
      }
    );
  }
}
