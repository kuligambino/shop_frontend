export interface RegisterForm {
  firstName?: string;
  lastName?: string;
  email?: string;
  password?: string;
  confirmedPassword?: string;
  phoneNumber?: string;
  birthDate?: Date;
  recaptcha?: boolean;
  approvals?: string[];
}
