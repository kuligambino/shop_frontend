export class Approval {
  constructor(
    public id?: number,
    public type?: string,
    public title?: string,
    public description?: string,
    public required?: boolean,
    public isShown: boolean = false,
    public isAccepted: boolean = false,
    public isTouched: boolean = false
  ) {}
}
