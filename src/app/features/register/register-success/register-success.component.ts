import { Component, OnInit } from "@angular/core";
import { DataService } from "src/app/shared/services/data.service";
import { of } from "rxjs";

@Component({
  selector: "app-register-success",
  templateUrl: "./register-success.component.html",
  styleUrls: ["./register-success.component.scss"],
})
export class RegisterSuccessComponent implements OnInit {
  userEmail$ = of("");

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.userEmail$ = this.dataService.currentEmail;
  }
}
