import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Approval } from "src/app/features/register/models/Approval";
import { RegisterForm } from "src/app/features/register/models/RegisterForm";
import { HttpService } from "../../../shared/services/http.service";
import { REGISTER_SUCCEED_URL } from "../../../global_properties";

@Injectable()
export class RegisterService {
  constructor(private readonly httpService: HttpService) {}

  getApprovals(): Observable<Approval[]> {
    return this.httpService.getApprovals();
  }

  emailExists(email: HTMLInputElement): Observable<boolean> {
    return this.httpService.checkEmailIfExist(email.value);
  }

  register(registerForm: RegisterForm): Observable<RegisterForm> {
    return this.httpService.sendRegisterForm(registerForm);
  }

  redirectToRegisterSucceed(): void {
    this.httpService.redirectToPage(REGISTER_SUCCEED_URL);
  }
}
