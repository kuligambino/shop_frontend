import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { NotLoggedGuard } from "src/app/shared/guards/not-logged.guard";
import { RegisterConfirmContainerComponent } from "./register-confirmation/register-confirm-container.component";
import { RegisterConfirmSuccessComponent } from "./register-confirmation/register-confirm-success/register-confirm-success.component";
import { RegisterConfirmComponent } from "./register-confirmation/register-confirm/register-confirm.component";
import { RegisterPageComponent } from "./register-page/register-page.component";
import { RegisterSuccessComponent } from "./register-success/register-success.component";
import { RegisterComponent } from "./register.component";

const routes: Routes = [
  {
    path: "",
    component: RegisterComponent,
    canActivate: [NotLoggedGuard],
    children: [
      {
        path: "",
        component: RegisterPageComponent,
      },
      {
        path: "succeed",
        component: RegisterSuccessComponent,
      },
      {
        path: "confirm",
        component: RegisterConfirmContainerComponent,
        children: [
          {
            path: "",
            component: RegisterConfirmComponent,
          },
          {
            path: "succeed",
            component: RegisterConfirmSuccessComponent,
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterRoutingModule {}
