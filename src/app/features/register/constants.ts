export const CONFIRM_REGISTER =
  'Potwierdź rejestrację klikając przycisk "Potwierdź rejestrację".';
export const CONFIRMED_REGISTER =
  "Super! Udało Ci się potwierdzić rejestrację! Możesz teraz w 100% korzystać ze swojego konta.";
