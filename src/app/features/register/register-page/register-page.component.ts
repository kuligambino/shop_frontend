import { Component, OnInit } from "@angular/core";
import { FormGroup, FormArray } from "@angular/forms";
import { RegisterForm } from "src/app/features/register/models/RegisterForm";
import {
  CREATE_ACCOUNT_BUTTON,
  EMAIL_EXISTS_VALIDATION,
  EMAIL_FORMAT_VALIDATION,
  FIELD_VALIDATION,
  LOGIN_BUTTON,
  PASSWORD_VALIDATION,
  PATTERN_VALIDATION,
  REGISTER_BUTTON,
  REGISTER_DESCRIPTION,
  REGISTER_MESSAGE,
  REGISTER_TITLE,
  RESET_FORM_BUTTON,
} from "src/app/global_properties";
import { ApiError } from "src/app/core/exceptions/ApiError";
import { HttpErrorResponse } from "@angular/common/http";
import { DataService } from "src/app/shared/services/data.service";
import { Approval } from "../models/Approval";
import { RegisterService } from "src/app/features/register/services/register.service";
import { FormService } from "src/app/shared/services/form.service";
import {
  acceptApproval,
  mapFormToModel,
  rejectApproval,
} from "src/app/features/register/helpers/register-helper";
import { first, switchMap } from "rxjs/operators";
import { of } from "rxjs";
import { mapHttpErrors } from "../../../shared/helpers/form-helpe";

@Component({
  selector: "app-register-page",
  templateUrl: "./register-page.component.html",
  styleUrls: ["./register-page.component.scss"],
  providers: [RegisterService],
})
export class RegisterPageComponent implements OnInit {
  registerDescription = REGISTER_DESCRIPTION;
  fieldValidation = FIELD_VALIDATION;
  emailValidation = EMAIL_FORMAT_VALIDATION;
  passwordValidation = PASSWORD_VALIDATION;
  patternValidation = PATTERN_VALIDATION;
  emailExistsValidation = EMAIL_EXISTS_VALIDATION;
  registerButton = REGISTER_BUTTON;
  resetFormButton = RESET_FORM_BUTTON;
  loginButton = LOGIN_BUTTON;
  createAccount = CREATE_ACCOUNT_BUTTON;
  registerTitle = REGISTER_TITLE;
  registerMessage = REGISTER_MESSAGE;

  registerFormGroup: FormGroup;
  registerForm: RegisterForm;
  errors: ApiError[] = [];
  approvals$ = of([]);
  emailExists$ = of(false);
  userEmail = "";
  passwordIsValid = false;

  constructor(
    private readonly dataService: DataService,
    private readonly registerService: RegisterService,
    private readonly formService: FormService
  ) {}

  ngOnInit(): void {
    this.approvals$ = this.registerService.getApprovals();
    this.registerFormGroup = this.formService.initRegisterForm();
  }

  submit(): void {
    this.registerForm = mapFormToModel(this.registerFormGroup);
    this.dataService.currentEmail
      .pipe(
        first(),
        switchMap((email: string) => {
          this.userEmail = email;
          return this.registerService.register(this.registerForm);
        })
      )
      .subscribe(() => {
        this.registerService.redirectToRegisterSucceed();
        this.registerFormGroup = this.formService.initRegisterForm();
      }),
      (httpErrors: HttpErrorResponse) => {
        this.errors = mapHttpErrors(httpErrors);
      };
  }

  acceptRecaptcha(): void {
    this.registerFormGroup.value.recaptcha = true;
  }

  emitEmail(email: HTMLInputElement): void {
    this.dataService.changeEmail(email.value);
  }

  emailExists(email: HTMLInputElement): void {
    this.emailExists$ = this.registerService.emailExists(email);
  }

  displayApprovalDescription(approval: Approval): void {
    approval.isShown = !approval.isShown;
  }

  isPasswordValid(event: boolean): void {
    this.passwordIsValid = event;
  }

  changeCheckbox(isChecked: boolean, approval: Approval): void {
    const approvals = this.registerFormGroup.get("approvals") as FormArray;
    if (isChecked) {
      acceptApproval(approvals, approval);
    } else {
      rejectApproval(approvals, approval);
    }
  }
}
