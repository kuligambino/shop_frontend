import { FormArray, FormControl, FormGroup } from "@angular/forms";
import { RegisterForm } from "src/app/features/register/models/RegisterForm";
import { Approval } from "../models/Approval";

export function mapFormToModel(registerFormGroup: FormGroup): RegisterForm {
  return {
    firstName: registerFormGroup.value.firstName,
    lastName: registerFormGroup.value.lastName,
    email: registerFormGroup.value.email,
    password: registerFormGroup.value.password,
    confirmedPassword: registerFormGroup.value.confirmedPassword,
    phoneNumber: registerFormGroup.value.phoneNumber,
    birthDate: registerFormGroup.value.birthDate,
    recaptcha: registerFormGroup.value.recaptcha,
    approvals: registerFormGroup.value.approvals,
  };
}

export function acceptApproval(approvals: FormArray, approval: Approval) {
  approvals.push(new FormControl(approval.type));
  approval.isAccepted = true;
  approval.isTouched = true;
}

export function rejectApproval(approvals: FormArray, approval: Approval) {
  const index = approvals.controls.findIndex((x) => x.value === approval.type);
  approvals.removeAt(index);
  approval.isAccepted = false;
}
