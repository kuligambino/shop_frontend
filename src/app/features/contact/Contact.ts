export interface Contact {
  email?: string;
  details?: string;
  grade?: number;
  recaptcha?: boolean;
}
