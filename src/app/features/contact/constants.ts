export const TITLE_CONTACT = "Formularz kontaktowy:";
export const MESSAGE_CONTACT = `
Czy masz jakieś pytania? Nie wahaj się skontaktować z nami bezpośrednio.
Nasz zespół skontaktuje się z Tobą, aby Ci pomóc.
`;
export const COMPANY_ADDRESS = "Plac Grunwaldzki 1, Wrocław 50-551, Polska";
export const COMPANY_PHONE = "+48 123 456 789";
export const COMPANY_EMAIL = "supplyshop@supplyshop.com";
export const LATITUDE = 51.1115919;
export const LONGITUDE = 17.0596862;
export const GRADES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
export const CONTACT_LABEL_EMAIL = "Email:";
export const CONTACT_LABEL_MESSAGE = "Wiadomość:";
export const CONTACT_LABEL_GRADE = "Ocena usługi:";
export const DETAILS_SHORT_VALIDATION = "Wiadomość musi mieć min. 50 znaków!";
