import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Contact } from "src/app/features/contact/Contact";
import { HttpService } from "src/app/shared/services/http.service";
import { HttpErrorResponse } from "@angular/common/http";
import {
  COMPANY_ADDRESS,
  COMPANY_EMAIL,
  COMPANY_PHONE,
  CONTACT_LABEL_EMAIL,
  CONTACT_LABEL_GRADE,
  CONTACT_LABEL_MESSAGE,
  GRADES,
  LATITUDE,
  LONGITUDE,
  MESSAGE_CONTACT,
  TITLE_CONTACT,
  DETAILS_SHORT_VALIDATION,
} from "src/app/features/contact/constants";
import { ApiError } from "src/app/core/exceptions/ApiError";
import { FormService } from "../../shared/services/form.service";
import { mapFormToModel, resetForm } from "./contact-helper";
import { mapHttpErrors } from "../../shared/helpers/form-helpe";
import {
  EMAIL_FORMAT_VALIDATION,
  FIELD_VALIDATION,
  SEND_BUTTON,
} from "../../global_properties";

@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.scss"],
})
export class ContactComponent implements OnInit {
  grades = GRADES;
  latitude = LATITUDE;
  longitude = LONGITUDE;
  companyAddress = COMPANY_ADDRESS;
  companyPhone = COMPANY_PHONE;
  companyEmail = COMPANY_EMAIL;
  title = TITLE_CONTACT;
  message = MESSAGE_CONTACT;
  emailValidation = EMAIL_FORMAT_VALIDATION;
  detailsValidation = DETAILS_SHORT_VALIDATION;
  fieldValidation = FIELD_VALIDATION;
  contactLabelEmail = CONTACT_LABEL_EMAIL;
  contactLabelMessage = CONTACT_LABEL_MESSAGE;
  contactLabelGrade = CONTACT_LABEL_GRADE;
  sendButton = SEND_BUTTON;

  errors: ApiError[] = [];
  contactForm: FormGroup;
  contact: Contact;

  constructor(
    private httpService: HttpService,
    private formService: FormService
  ) {}

  ngOnInit(): void {
    this.contactForm = this.formService.initContactForm();
  }

  submit() {
    this.contact = mapFormToModel(this.contactForm);
    this.httpService.sendContactForm(this.contact).subscribe(
      () => {},
      (httpErrors: HttpErrorResponse) => {
        this.errors = mapHttpErrors(httpErrors);
      }
    );
    resetForm(this.contactForm);
  }

  resolveRecaptcha() {
    this.contactForm.value.recaptcha = true;
  }
}
