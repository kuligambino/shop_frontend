import { FormGroup } from "@angular/forms";
import { Contact } from "./Contact";
import { GRADES } from "src/app/features/contact/constants";

export function mapFormToModel(contactMessage: FormGroup): Contact {
  return {
    email: contactMessage.value.email,
    details: contactMessage.value.details,
    grade: contactMessage.value.grade,
    recaptcha: contactMessage.value.recaptcha,
  };
}

export function resetForm(contactForm: FormGroup) {
  contactForm.reset({
    grade: GRADES[4],
  });
}
