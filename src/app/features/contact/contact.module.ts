import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AgmCoreModule } from "@agm/core";

import { ContactRoutingModule } from "./contact-routing.module";
import { ContactComponent } from "./contact.component";
import { HttpClientModule } from "@angular/common/http";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [ContactComponent],
  imports: [
    CommonModule,
    ContactRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyB-uKlMO6BNy0D-ARQGbbrOrHY1cmpw7C0",
    }),
    HttpClientModule,
    SharedModule,
  ],
  exports: [ContactComponent],
})
export class ContactModule {}
