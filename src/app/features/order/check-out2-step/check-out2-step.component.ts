import { Component, OnInit } from "@angular/core";
import { Address } from "./models/Address";
import { CreditCard } from "./models/CreditCard";
import { Order } from "../model/Order";
import { OrderService } from "../../../shared/services/order.service";
import { HttpService } from "../../../shared/services/http.service";
import { DataService } from "../../../shared/services/data.service";
import { ActivatedRoute } from "@angular/router";
import { map } from "rxjs/operators";
import { Invoice } from "./models/Invoice";

@Component({
  selector: "app-check-out2-step",
  templateUrl: "./check-out2-step.component.html",
  styleUrls: ["./check-out2-step.component.scss"],
})
export class CheckOut2StepComponent implements OnInit {
  address: Address;
  addresses: Address[] = [];
  creditCard: CreditCard;
  invoice: Invoice;
  step = 0;
  paymentConfirmed = false;
  order: Order;
  orderProblemMessage = "";
  requireInvoice = false;

  constructor(
    private orderService: OrderService,
    private httpService: HttpService,
    private dataService: DataService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.pipe(
      map((data) => (this.addresses = data.addresses))
    );
    this.httpService.getOrder().subscribe((order: Order) => {
      if (order.address != null) {
        this.address = order.address;
        this.step = 1;
      } else {
        this.httpService.getAddresses().subscribe((addresses: Address[]) => {
          const mainAddress = addresses.find((address) => address.main);
          if (mainAddress != null) {
            this.address = mainAddress;
            this.step = 1;
          } else {
            this.address = new Address();
          }
        });
      }
    });
  }

  setStep(index: number): void {
    this.step = index;
  }

  setDelivery(event: Address): void {
    this.address = event;
  }

  setPayment(event: CreditCard): void {
    this.creditCard = event;
  }

  cardIsValidated(cardIsValidated: boolean): void {
    this.paymentConfirmed = cardIsValidated;
  }

  placeOrder(regulationAccepted: boolean) {
    if (regulationAccepted) {
      this.order.address = this.address;
      this.order.creditCard = this.creditCard;
      this.order.invoice = this.invoice;
      this.order.regulationAccepted = regulationAccepted;
      return this.httpService.placeOrder(this.order).subscribe(() => {
        this.dataService.emitOrder(this.order);
        this.orderService.deleteCookie("order");
        this.httpService.redirectToPage("order/succeed");
      });
    } else {
      this.orderProblemMessage = "Problem z zamówieniem!";
    }
  }

  setCurrentOrder(order: Order) {
    this.order = order;
  }

  setInvoice(invoice: Invoice) {
    this.invoice = invoice;
  }
}
