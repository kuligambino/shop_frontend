export interface CreditCard {
  ownerFirstName?: string;
  ownerLastName?: string;
  cardNumber?: string;
  expirationMonth?: string;
  expirationYear?: string;
  cvv?: string;
}
