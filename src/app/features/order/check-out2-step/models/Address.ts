export class Address {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public email?: string,
    public phoneNumber?: string,
    public street?: string,
    public city?: string,
    public postCode?: string,
    public country?: string,
    public main?: boolean
  ) {}
}
