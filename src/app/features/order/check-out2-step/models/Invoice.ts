export interface Invoice {
  companyName: string;
  nip: string;
  companyAddress: string;
}
