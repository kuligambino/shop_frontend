import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: "app-panel-header",
  templateUrl: "./panel-header.component.html",
  styleUrls: ["./panel-header.component.scss"],
})
export class PanelHeaderComponent {
  @Input() step = 0;
  @Input() title = "";
  @Input() index = 0;
  @Output() stepEmitter = new EventEmitter();

  setStep(index: number): void {
    this.step = index;
    this.stepEmitter.emit(this.step);
  }
}
