import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { Address } from "../models/Address";
import { HttpService } from "../../../../shared/services/http.service";

@Injectable()
export class AddressResolver implements Resolve<Address[]> {
  constructor(private readonly httpService: HttpService) {}

  resolve(): Observable<Address[]> {
    return this.httpService.getAddresses();
  }
}
