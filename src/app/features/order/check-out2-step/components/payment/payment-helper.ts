import { FormGroup } from "@angular/forms";
import { CreditCard } from "../../models/CreditCard";

export function mapFormToModel(paymentFormGroup: FormGroup): CreditCard {
  return {
    ownerFirstName: paymentFormGroup.value.ownerFirstName,
    ownerLastName: paymentFormGroup.value.ownerLastName,
    cardNumber: paymentFormGroup.value.cardNumber,
    expirationMonth: paymentFormGroup.value.expirationMonth,
    expirationYear: paymentFormGroup.value.expirationYear,
    cvv: paymentFormGroup.value.cvv,
  };
}
