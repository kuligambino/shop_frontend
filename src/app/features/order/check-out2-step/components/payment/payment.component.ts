import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormGroup } from "@angular/forms";
import {
  CREDIT_CARD_PATTERN_VALIDATION,
  CVV_PATTERN_VALIDATION,
  FIELD_VALIDATION,
  PAYMENT_ERROR,
} from "../../../../../global_properties";
import { CreditCard } from "../../models/CreditCard";
import { HttpService } from "../../../../../shared/services/http.service";
import { ApiError } from "src/app/core/exceptions/ApiError";
import { FormService } from "../../../../../shared/services/form.service";
import { mapFormToModel } from "./payment-helper";
import { MONTHS, YEARS } from "../../../constants";

@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.scss"],
})
export class PaymentComponent implements OnInit {
  fieldValidation = FIELD_VALIDATION;
  creditCardPatternValidation = CREDIT_CARD_PATTERN_VALIDATION;
  cvvPatternValidation = CVV_PATTERN_VALIDATION;
  months = MONTHS;
  years = YEARS;
  paymentError = PAYMENT_ERROR;

  @Output() changedStep: EventEmitter<number> = new EventEmitter<number>();
  @Output() paymentEmitter: EventEmitter<CreditCard> =
    new EventEmitter<CreditCard>();
  @Output() creditCardEmitter: EventEmitter<boolean> =
    new EventEmitter<boolean>();
  paymentConfirmed = false;
  errors: ApiError[] = [];
  step = 1;
  serverNotApprovedMessage = "";
  paymentFormGroup: FormGroup;
  payment: CreditCard;

  constructor(
    private formService: FormService,
    private httpService: HttpService
  ) {}

  ngOnInit(): void {
    this.paymentFormGroup = this.formService.initPaymentForm();
  }

  submit(): void {
    this.payment = mapFormToModel(this.paymentFormGroup);
    this.validateCreditCard();
    this.paymentEmitter.emit(this.payment);
  }

  validateCreditCard(): void {
    this.httpService.validatePayment(this.payment).subscribe(
      (paymentConfirmed: boolean) => {
        this.paymentConfirmed = paymentConfirmed;
        this.increaseStep();
      },
      () => {
        this.paymentConfirmed = false;
        this.serverNotApprovedMessage = this.paymentError;
      }
    );
  }

  increaseStep(): void {
    this.step++;
    this.changedStep.emit(this.step);
    this.creditCardEmitter.emit(this.paymentConfirmed);
  }
}
