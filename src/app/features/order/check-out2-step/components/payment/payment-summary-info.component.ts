import { Component, Input } from "@angular/core";
import { CreditCard } from "../../models/CreditCard";

@Component({
  selector: "app-payment-summary-info",
  templateUrl: "./payment-summary-info.component.html",
  styleUrls: ["./payment-summary-info.component.scss"],
})
export class PaymentSummaryInfoComponent {
  @Input() step = 0;
  @Input() creditCard: CreditCard;
  @Input() paymentConfirmed = false;
}
