import { Component, EventEmitter, Output } from "@angular/core";
import { ApiError } from "src/app/core/exceptions/ApiError";
import { FIELD_VALIDATION } from "../../../../../global_properties";

@Component({
  selector: "app-regulation",
  templateUrl: "./regulation.component.html",
  styleUrls: ["./regulation.component.scss"],
})
export class RegulationComponent {
  fieldValidation = FIELD_VALIDATION;

  regulationIsAccepted: boolean;
  isTouched: boolean;
  errors: ApiError[] = [];
  @Output() regulationEmitter: EventEmitter<boolean> =
    new EventEmitter<boolean>();

  acceptRegulationPolicy(): void {
    this.regulationIsAccepted = !this.regulationIsAccepted;
    this.isTouched = true;
  }

  submitCheckout(): void {
    this.sendRegulationAcceptance();
  }

  sendRegulationAcceptance(): void {
    this.regulationEmitter.emit(this.regulationIsAccepted);
  }
}
