import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { Order } from "../../../model/Order";
import { HttpService } from "../../../../../shared/services/http.service";

@Component({
  selector: "app-order-summary",
  templateUrl: "./order-summary.component.html",
  styleUrls: ["./order-summary.component.scss"],
})
export class OrderSummaryComponent implements OnInit {
  currentOrder: Order;
  @Output() orderEmitter: EventEmitter<Order> = new EventEmitter<Order>();

  constructor(private httpService: HttpService) {}

  ngOnInit(): void {
    this.httpService.getOrder().subscribe((order: Order) => {
      this.currentOrder = order;
      this.orderEmitter.emit(this.currentOrder);
    });
  }
}
