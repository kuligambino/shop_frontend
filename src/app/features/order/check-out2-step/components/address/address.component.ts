import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { Address } from "../../models/Address";
import { AuthService } from "../../../../../shared/services/auth.service";
import { Invoice } from "../../models/Invoice";

@Component({
  selector: "app-address",
  templateUrl: "./address.component.html",
  styleUrls: ["./address.component.scss"],
})
export class AddressComponent implements OnInit {
  @Output() changeStep: EventEmitter<number> = new EventEmitter<number>();
  @Output() addressEmitter: EventEmitter<Address> = new EventEmitter<Address>();
  @Output() invoiceEmitter: EventEmitter<Invoice> = new EventEmitter<Invoice>();
  @Input() address: Address;
  @Input() addresses: Address[] = [];
  step = 0;
  isAuthenticated: boolean;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.isAuthenticated = this.authService.isAuthenticated();
  }

  setStep(index: number): void {
    this.step = index;
    this.changeStep.emit(this.step);
  }

  setDelivery(address: Address): void {
    this.address = address;
    this.addressEmitter.emit(this.address);
  }

  setInvoice(invoice: Invoice) {
    this.invoiceEmitter.emit(invoice);
  }
}
