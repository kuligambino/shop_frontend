import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { FormGroup } from "@angular/forms";
import {
  EMAIL_FORMAT_VALIDATION,
  FIELD_VALIDATION,
  PHONE_FORMAT_VALIDATION,
  POST_CODE_FORMAT_VALIDATION,
  WRONGLY_FILLED_FORM,
} from "../../../../../../global_properties";
import { StoreService } from "../../../../../../shared/services/store.service";
import { HttpService } from "../../../../../../shared/services/http.service";
import { OrderService } from "../../../../../../shared/services/order.service";
import { Address } from "../../../models/Address";
import { AuthService } from "../../../../../../shared/services/auth.service";
import { MatDialog } from "@angular/material/dialog";
import { ApiError } from "src/app/core/exceptions/ApiError";
import { ORDER_COOKIE_NAME } from "../../../../constants";
import { FormService } from "../../../../../../shared/services/form.service";
import { COUNTRIES } from "../../../../../../shared/data/countries";
import { InvoiceComponent } from "../../invoice/invoice.component";
import { Invoice } from "../../../models/Invoice";

@Component({
  selector: "app-address-form",
  templateUrl: "./address-form.component.html",
  styleUrls: ["./address-form.component.scss"],
})
export class AddressFormComponent implements OnInit {
  fieldValidation = FIELD_VALIDATION;
  emailValidation = EMAIL_FORMAT_VALIDATION;
  phoneNumberValidation = PHONE_FORMAT_VALIDATION;
  postCodeValidation = POST_CODE_FORMAT_VALIDATION;
  wronglyFilledForm = WRONGLY_FILLED_FORM;
  countries = COUNTRIES;
  addressFormGroup: FormGroup;
  errors: ApiError[] = [];
  notValidatedFormMessage = "";
  serverApproved: boolean;
  isAuthenticated: boolean;
  addresses: Address[] = [];
  step = 0;
  @Output() changeStep: EventEmitter<number> = new EventEmitter<number>();
  @Output() addressEmitter: EventEmitter<Address> = new EventEmitter<Address>();
  @Output() invoiceEmitter: EventEmitter<Invoice> = new EventEmitter<Invoice>();
  @Output() addressesEmitter: EventEmitter<Address[]> = new EventEmitter<
    Address[]
  >();
  @Input() address: Address;
  @ViewChild("invoice") invoice: InvoiceComponent;
  requireInvoice = false;
  invoiceModel: Invoice;
  isInvoiceValid = false;

  constructor(
    private storeService: StoreService,
    private httpService: HttpService,
    private orderService: OrderService,
    private authService: AuthService,
    private dialog: MatDialog,
    private formService: FormService
  ) {
    this.isAuthenticated = this.authService.isAuthenticated();
  }

  ngOnInit() {
    this.addressFormGroup = this.formService.initDelivery(this.address);
  }

  submit(): void {
    this.setValuesFromForm();
    this.assignAddressToOrder(this.address);
    if (this.step === 0 && this.serverApproved === false) {
      this.notValidatedFormMessage = this.wronglyFilledForm;
    }
    if (this.requireInvoice && this.invoice.invoiceForm.valid) {
      this.invoiceModel = this.invoice.invoiceForm.value;
      this.assignInvoiceToOrder(this.invoiceModel);
    }
    this.addressEmitter.emit(this.address);
    this.invoiceEmitter.emit(this.invoiceModel);
  }

  setValuesFromForm(): void {
    this.address.firstName = this.addressFormGroup.value.firstName;
    this.address.lastName = this.addressFormGroup.value.lastName;
    this.address.email = this.addressFormGroup.value.email;
    this.address.phoneNumber = this.addressFormGroup.value.phoneNumber;
    this.address.street = this.addressFormGroup.value.street;
    this.address.city = this.addressFormGroup.value.city;
    this.address.postCode = this.addressFormGroup.value.postCode;
    this.address.country = this.addressFormGroup.value.country;
  }

  assignAddressToOrder(address: Address): void {
    const orderId = this.orderService.getCookie(ORDER_COOKIE_NAME);
    this.httpService
      .assignAddressToOrder(address, orderId)
      .subscribe((updatedAddress: Address) => {
        if (updatedAddress != null) {
          this.step = 1;
          this.serverApproved = true;
          this.changeStep.emit(this.step);
          this.address = updatedAddress;
        } else {
          this.step = 0;
          this.serverApproved = false;
        }
      });
  }

  assignInvoiceToOrder(invoice: Invoice): void {
    this.httpService.validateInvoice(invoice).subscribe((isValidated) => {
      if (!isValidated) {
        this.serverApproved = false;
      }
    });
  }

  addAddress(address: Address) {
    this.setValuesFromForm();
    this.httpService.addAddress(address).subscribe((addresses: Address[]) => {
      this.addresses = addresses;
      this.addressesEmitter.emit(this.addresses);
    });
  }

  editAddress(address: Address) {
    this.setValuesFromForm();
    this.httpService.editAddress(address).subscribe((addresses: Address[]) => {
      this.addresses = addresses;
      this.addressesEmitter.emit(this.addresses);
    });
  }

  closeDialog() {
    this.dialog.closeAll();
  }

  changeCheckbox() {
    this.requireInvoice = !this.requireInvoice;
  }

  isValid(isValid: boolean) {
    this.isInvoiceValid = isValid;
  }
}
