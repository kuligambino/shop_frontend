import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { AddressFormDialogComponent } from "../address-form-dialog/address-form-dialog.component";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { HttpErrorResponse } from "@angular/common/http";
import { HttpService } from "src/app/shared/services/http.service";
import { OrderService } from "src/app/shared/services/order.service";
import { Address } from "../../../models/Address";
import { ORDER_COOKIE_NAME } from "../../../../constants";

@Component({
  selector: "app-address-list",
  templateUrl: "./address-list.component.html",
  styleUrls: ["./address-list.component.scss"],
})
export class AddressListComponent implements OnInit {
  @Output() changeStep: EventEmitter<number> = new EventEmitter();
  @Output() addressEmitter: EventEmitter<Address> = new EventEmitter();
  addresses: Address[] = [];
  step = 0;
  mainAddress: Address;
  chosenAddress: Address;
  selectedAddress: boolean;
  serverApproved: boolean;
  clickedButton: number;
  addressesError = "";

  constructor(
    private dialog: MatDialog,
    private httpService: HttpService,
    private orderService: OrderService
  ) {}

  ngOnInit(): void {
    this.httpService.getAddresses().subscribe(
      (addresses: Address[]) => {
        this.addresses = addresses;
        if (this.hasMainAddress()) {
          this.chosenAddress = this.getMainAddress();
          const orderId = this.orderService.getCookie(ORDER_COOKIE_NAME);
          this.httpService.assignAddressToOrder(this.chosenAddress, orderId);
        }
      },
      (error: HttpErrorResponse) => {
        this.addressesError = error.message;
      },
      () => {
        this.mainAddress = this.getMainAddress();
      }
    );
  }

  getMainAddress(): Address {
    return this.addresses.find((address: Address) => address.main);
  }

  hasMainAddress(): boolean {
    return this.addresses.filter((address: Address) => address.main).length > 0;
  }

  editAddress(address: Address): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.hasBackdrop = true;
    dialogConfig.autoFocus = true;
    dialogConfig.backdropClass = "backdropBackground";
    dialogConfig.data = {
      address,
    };
    const dialogRef = this.dialog.open(
      AddressFormDialogComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((result) => {
      this.addresses = result.data;
    });
  }

  deleteAddress(address: Address, buttonNumber: number): void {
    if (buttonNumber === this.clickedButton) {
      this.clickedButton = null;
      this.selectedAddress = false;
    } else if (buttonNumber < this.clickedButton) {
      this.clickedButton--;
    }
    this.httpService
      .deleteAddress(address.id)
      .subscribe((addresses: Address[]) => {
        this.addresses = addresses;
      });
  }

  addNewAddress(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.hasBackdrop = true;
    dialogConfig.autoFocus = true;
    dialogConfig.backdropClass = "backdropBackground";
    dialogConfig.data = {
      address: new Address(),
    };
    const dialogRef = this.dialog.open(
      AddressFormDialogComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((result) => {
      this.addresses = result.data;
    });
  }

  goToPayment(): void {
    const orderId = this.orderService.getCookie(ORDER_COOKIE_NAME);
    this.httpService
      .assignAddressToOrder(this.chosenAddress, orderId)
      .subscribe((updatedAddress: Address) => {
        if (updatedAddress != null) {
          this.step = 1;
          this.serverApproved = true;
          this.changeStep.emit(this.step);
          this.addressEmitter.emit(this.chosenAddress);
        } else {
          this.step = 0;
          this.serverApproved = false;
        }
      });
  }

  selectAddress(address: Address, buttonNumber: number): void {
    this.chosenAddress = address;
    this.selectedAddress = true;
    this.clickedButton = buttonNumber;
  }
}
