import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { Address } from "../../../models/Address";

@Component({
  selector: "app-address-form-dialog",
  templateUrl: "address-form-dialog.html",
  styleUrls: ["./address-form-dialog.scss"],
})
export class AddressFormDialogComponent {
  addressToEdit: Address;
  updatedAddresses: Address[] = [];

  constructor(
    private dialogRef: MatDialogRef<AddressFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.addressToEdit = data.address;
  }

  updateAddress(): void {
    this.onNoClick();
  }

  setAddresses(addresses: Address[]): void {
    this.updatedAddresses = addresses;
    this.dialogRef.close({ event: "close", data: this.updatedAddresses });
  }

  private onNoClick(): void {
    this.dialogRef.close();
  }
}
