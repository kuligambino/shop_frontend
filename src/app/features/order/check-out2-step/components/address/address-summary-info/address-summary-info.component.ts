import { Component, Input } from "@angular/core";
import { Address } from "../../../models/Address";
import { Invoice } from "../../../models/Invoice";

@Component({
  selector: "app-address-summary-info",
  templateUrl: "./address-summary-info.component.html",
  styleUrls: ["./address-summary-info.component.scss"],
})
export class AddressSummaryInfoComponent {
  @Input() address: Address;
  @Input() invoice: Invoice;
  @Input() step = 0;
}
