import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormService } from "../../../../../shared/services/form.service";
import { FormGroup } from "@angular/forms";
import {
  FIELD_VALIDATION,
  NIP_PATTERN_VALIDATION,
} from "../../../../../global_properties";

@Component({
  selector: "app-invoice",
  templateUrl: "./invoice.component.html",
  styleUrls: ["./invoice.component.scss"],
})
export class InvoiceComponent implements OnInit {
  fieldValidation = FIELD_VALIDATION;
  nipValidation = NIP_PATTERN_VALIDATION;
  invoiceForm: FormGroup;
  @Output() isValid: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private formService: FormService) {}

  ngOnInit(): void {
    this.invoiceForm = this.formService.initInvoiceForm();
    this.elo();
  }

  elo() {
    this.invoiceForm.statusChanges.subscribe((isValid) =>
      this.isValid.emit(isValid === "VALID")
    );
  }
}
