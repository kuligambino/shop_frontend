export const EMPTY_CART = "Koszyk jest pusty!";
export const ORDER_COOKIE_NAME = "order";
export const DELIVERY_DATE_INFORMATION = `
    Podane daty wysyłki są przybliżone. Dokładne terminy zależą od dostępności produktów.
    Przeciętnie dostawa produktów, które są na stanie, trwa od trzech do pięciu dni roboczych.
    Termin dostawy produktów, których nie ma w danej chwili na stanie i muszą zostać dodatkowo zamówione,
    może być dłuższy. Użytkownik może śledzić zamówienia przy użyciu łącza z systemu śledzenia przesyłek (Track
    Trace), które jest wysyłane w wiadomości e-mail po wysłaniu zamówienia.`;
export const PAYMENT_METHODS = `
    W sklepie akceptowane są następujące metody płatności:
    Master Card, Visa, Dotpay, Maestro
    Płatności są przetwarzane przez zewnętrznego usługodawcę, firmę Adyen.`;
export const RETURN_REQUIREMENTS = `
    Nabywca może zwrócić produkt w ciągu 14 dni od jego otrzymania. Można zwrócić całość
    zamówienia
    lub jego część.
    Produkty muszą być nowe, nieużywane, w oryginalnym opakowaniu i z oryginalnymi akcesoriami.
    Więcej informacji można znaleźć w sekcji ZASADY ZWROTÓW.`;
export const RETURN_PROCEDURE = `
    W razie decyzji o zwrocie zamówienia należy zadzwonić do działu obsługi klienta w sklepie
    firmy KKWJ w celu złożenia zgłoszenia zwrotu i uzyskania numeru RMA.`;
export const FREE_RETURNS = `Zwrot produktów jest prosty i bezpłatny.`;
export const MONTHS = [
  "Styczeń",
  "Luty",
  "Marzec",
  "Kwiecień",
  "Maj",
  "Czerwiec",
  "Lipiec",
  "Sierpień",
  "Wrzesień",
  "Październik",
  "Listopad",
  "Grudzień",
];

export const YEARS = [
  "2020",
  "2021",
  "2022",
  "2023",
  "2024",
  "2025",
  "2026",
  "2027",
  "2028",
  "2029",
  "2030",
  "2031",
  "2032",
];
