import { Component, Input } from "@angular/core";
import { Order } from "../model/Order";
import { CartService } from "./cart.service";

@Component({
  selector: "app-not-empty-cart",
  templateUrl: "./not-empty-cart.component.html",
  styleUrls: ["./not-empty-cart.component.scss"],
  providers: [CartService],
})
export class NotEmptyCartComponent {
  @Input() currentOrder: Order;
  quantity: number = 0;

  constructor(private cartService: CartService) {}

  deleteFromOrder(productId: number): void {
    this.cartService.deleteFromOrder(productId, this.currentOrder);
  }

  subtractFromOrder(productId: number): void {
    this.cartService.subtractFromOrder(productId, this.currentOrder);
  }

  addToOrder(productId: number): void {
    this.cartService.addToOrder(productId, this.currentOrder);
  }
}
