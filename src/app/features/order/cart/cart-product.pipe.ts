import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "cartProduct",
})
export class CartProductPipe implements PipeTransform {
  private MAX_PRODUCTS = 9;
  private MIN_PRODUCTS = 1;

  transform(value: number): number {
    if (value >= this.MIN_PRODUCTS && value <= this.MAX_PRODUCTS) {
      return value;
    } else if (value > this.MAX_PRODUCTS) {
      return this.MAX_PRODUCTS;
    } else if (value < this.MIN_PRODUCTS) {
      return this.MIN_PRODUCTS;
    }
  }
}
