import { Injectable } from "@angular/core";
import { OrderProduct } from "../model/OrderProduct";
import { Order } from "../model/Order";
import { HttpService } from "../../../shared/services/http.service";
import { QuantityUpdateService } from "../../../shared/services/quantity-update.service";

@Injectable()
export class CartService {
  constructor(
    private httpService: HttpService,
    private quantityUpdateService: QuantityUpdateService
  ) {}

  deleteFromOrder(productId: number, currentOrder: Order): void {
    currentOrder.orderProducts = currentOrder.orderProducts.filter(
      (orderProduct: OrderProduct) => orderProduct.id !== productId
    );
    this.updateOrder(currentOrder);
    currentOrder.amount = CartService.recalculatePrice(currentOrder);
  }

  subtractFromOrder(productId: number, currentOrder: Order): void {
    currentOrder.orderProducts.forEach((orderProduct: OrderProduct) => {
      if (orderProduct.id === productId) {
        if (orderProduct.quantity === 1) {
          this.deleteFromOrder(productId, currentOrder);
        }
        if (orderProduct.quantity > 1) {
          orderProduct.quantity--;
          this.updateOrder(currentOrder);
        }
      }
    });
    currentOrder.amount = CartService.recalculatePrice(currentOrder);
  }

  addToOrder(productId: number, currentOrder: Order): void {
    currentOrder.orderProducts.forEach((orderProduct: OrderProduct) => {
      if (orderProduct.id === productId && orderProduct.quantity < 9) {
        orderProduct.quantity++;
      }
    });
    this.updateOrder(currentOrder);
    currentOrder.amount = CartService.recalculatePrice(currentOrder);
  }

  private static recalculatePrice(order: Order): number {
    return order.orderProducts
      .map((product) => product.price * product.quantity)
      .reduce((a, b) => a + b, 0);
  }

  private updateOrder(order: Order): void {
    this.httpService.updateOrder(order).subscribe((updatedOrder: Order) => {
      order = updatedOrder;
      this.quantityUpdateService.triggerQuantity();
    });
  }
}
