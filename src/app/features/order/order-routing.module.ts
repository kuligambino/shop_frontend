import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CheckOut1StepComponent } from "./check-out1-step/check-out1-step.component";
import { CheckOut2StepComponent } from "./check-out2-step/check-out2-step.component";
import { OrderSuccessComponent } from "./order-success/order-success.component";
import { OrderPageComponent } from "./order-page.component";
import { CheckOut1CanActivateGuard } from "../../shared/guards/check-out1-can-activate-guard";
import { FinishOrderCanActivateGuard } from "../../shared/guards/finish-order-can-activate-guard";
import { CheckOut2CanActivateGuard } from "../../shared/guards/check-out2-can-activate.guard";
import { AddressResolver } from "./check-out2-step/resolvers/address.resolver";
import { OrderResolver } from "./resolvers/order.resolver";

const routes: Routes = [
  {
    path: "",
    component: OrderPageComponent,
    resolve: { order: OrderResolver },
  },
  {
    path: "checkout-1",
    canActivate: [CheckOut1CanActivateGuard],
    component: CheckOut1StepComponent,
  },
  {
    path: "checkout-2",
    canActivate: [CheckOut2CanActivateGuard],
    component: CheckOut2StepComponent,
    resolve: { addresses: AddressResolver },
  },
  {
    path: "succeed",
    canActivate: [FinishOrderCanActivateGuard],
    component: OrderSuccessComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderRoutingModule {}
