import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatExpansionModule } from "@angular/material/expansion";

import { OrderRoutingModule } from "./order-routing.module";
import { OrderPageComponent } from "./order-page.component";
import { EmptyCartComponent } from "./cart/empty-cart.component";
import { OrderSuccessComponent } from "./order-success/order-success.component";
import { CheckOut1StepComponent } from "./check-out1-step/check-out1-step.component";
import { CheckOut2StepComponent } from "./check-out2-step/check-out2-step.component";
import { AddressComponent } from "./check-out2-step/components/address/address.component";
import { AddressFormComponent } from "./check-out2-step/components/address/address-form/address-form.component";
import { PaymentComponent } from "./check-out2-step/components/payment/payment.component";
import { RegulationComponent } from "./check-out2-step/components/regulation/regulation.component";
import { SharedModule } from "src/app/shared/shared.module";
import { OrderSummaryComponent } from "./check-out2-step/components/order-summary/order-summary.component";
import { AddressFormDialogComponent } from "./check-out2-step/components/address/address-form-dialog/address-form-dialog.component";
import { AddressListComponent } from "./check-out2-step/components/address/address-list/address-list.component";
import { OrderInformationComponent } from "./check-out1-step/order-information.component";
import { InvoiceComponent } from "./check-out2-step/components/invoice/invoice.component";
import { NotEmptyCartComponent } from "./cart/not-empty-cart.component";
import { OrderResolver } from "./resolvers/order.resolver";
import { CartPaymentComponent } from "./components/cart-payment.component";
import { CartProductComponent } from "./components/cart-product.component";
import { CartProductPipe } from "./cart/cart-product.pipe";
import { GuestOrderComponent } from "./check-out1-step/guest-order.component";
import { PanelHeaderComponent } from "./check-out2-step/panel-header/panel-header.component";
import { AddressSummaryInfoComponent } from "./check-out2-step/components/address/address-summary-info/address-summary-info.component";
import { PaymentSummaryInfoComponent } from "./check-out2-step/components/payment/payment-summary-info.component";
import {AddressResolver} from './check-out2-step/resolvers/address.resolver';

@NgModule({
    declarations: [
        OrderPageComponent,
        EmptyCartComponent,
        NotEmptyCartComponent,
        OrderSuccessComponent,
        OrderSummaryComponent,
        CheckOut1StepComponent,
        CheckOut2StepComponent,
        AddressComponent,
        AddressFormComponent,
        AddressFormDialogComponent,
        AddressListComponent,
        OrderSuccessComponent,
        PaymentComponent,
        RegulationComponent,
        OrderInformationComponent,
        InvoiceComponent,
        CartPaymentComponent,
        CartProductComponent,
        CartProductPipe,
        GuestOrderComponent,
        PanelHeaderComponent,
        AddressSummaryInfoComponent,
        PaymentSummaryInfoComponent,
    ],
    imports: [CommonModule, OrderRoutingModule, SharedModule, MatExpansionModule],
    exports: [
        OrderPageComponent,
        EmptyCartComponent,
        OrderSummaryComponent,
        OrderSuccessComponent,
        CheckOut1StepComponent,
        CheckOut2StepComponent,
        AddressComponent,
        AddressFormComponent,
        AddressFormDialogComponent,
        AddressListComponent,
        OrderSuccessComponent,
        PaymentComponent,
        RegulationComponent,
    ],
    providers: [OrderResolver, AddressResolver]
})
export class OrderModule {}
