import { Component, EventEmitter, Input, Output } from "@angular/core";
import { OrderProduct } from "../model/OrderProduct";

@Component({
  selector: "app-cart-product",
  templateUrl: "./cart-product.component.html",
  styleUrls: ["./cart-product.component.scss"],
})
export class CartProductComponent {
  @Input() product: OrderProduct;
  @Input() finishedOrder: boolean;
  @Output() subtract: EventEmitter<number> = new EventEmitter<number>();
  @Output() add: EventEmitter<number> = new EventEmitter<number>();
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();

  deleteFromOrder(productId: number): void {
    this.delete.emit(productId);
  }

  subtractFromOrder(productId: number): void {
    this.subtract.emit(productId);
  }

  addToOrder(productId: number): void {
    this.add.emit(productId);
  }
}
