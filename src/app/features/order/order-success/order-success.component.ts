import { Component, OnInit } from "@angular/core";
import { DataService } from "src/app/shared/services/data.service";
import { Order } from "../model/Order";
import { QuantityUpdateService } from "../../../shared/services/quantity-update.service";

@Component({
  selector: "app-order-success",
  templateUrl: "./order-success.component.html",
  styleUrls: ["./order-success.component.scss"],
})
export class OrderSuccessComponent implements OnInit {
  currentOrder: Order;
  email = "";

  constructor(
    private dataService: DataService,
    private quantityUpdateService: QuantityUpdateService
  ) {}

  ngOnInit(): void {
    this.dataService.currentOrder.subscribe((order: Order) => {
      this.currentOrder = order;
      this.email = order.address.email;
      this.quantityUpdateService.triggerQuantity();
    });
  }
}
