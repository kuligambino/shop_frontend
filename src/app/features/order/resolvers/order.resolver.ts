import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { HttpService } from "../../../shared/services/http.service";
import { Order } from "../model/Order";

@Injectable()
export class OrderResolver implements Resolve<Order> {
  constructor(private httpService: HttpService) {}

  resolve(): Observable<Order> {
    return this.httpService.getOrder();
  }
}
