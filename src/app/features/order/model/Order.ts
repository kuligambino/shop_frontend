import { OrderProduct } from "./OrderProduct";
import { Address } from "../check-out2-step/models/Address";
import { CreditCard } from "../check-out2-step/models/CreditCard";
import { Invoice } from "../check-out2-step/models/Invoice";

export interface Order {
  id: number;
  orderProducts: OrderProduct[];
  amount: number;
  address: Address;
  creditCard: CreditCard;
  invoice?: Invoice;
  regulationAccepted: boolean;
}
