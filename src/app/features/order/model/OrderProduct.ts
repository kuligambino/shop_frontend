export interface OrderProduct {
  id?: number;
  name?: string;
  weight?: number;
  price?: number;
  taste?: string;
  categoryName?: string;
  img?: string;
  quantity?: number;
}
