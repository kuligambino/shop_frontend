import { Component, OnInit } from "@angular/core";
import { Order } from "./model/Order";
import { Observable } from "rxjs/internal/Observable";
import { ActivatedRoute } from "@angular/router";
import { switchMap } from "rxjs/operators";
import { of } from "rxjs";

@Component({
  selector: "app-order-page",
  templateUrl: "./order-page.component.html",
  styleUrls: ["./order-page.component.scss"],
})
export class OrderPageComponent implements OnInit {
  currentOrder$: Observable<Order>;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.currentOrder$ = this.activatedRoute.data.pipe(
      switchMap((data) => of(data.order))
    );
  }
}
