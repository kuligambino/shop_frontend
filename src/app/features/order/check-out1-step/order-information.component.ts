import { Component } from "@angular/core";
import {
  DELIVERY_DATE_INFORMATION,
  FREE_RETURNS,
  PAYMENT_METHODS,
  RETURN_PROCEDURE,
  RETURN_REQUIREMENTS,
} from "../constants";

@Component({
  selector: "app-order-information",
  templateUrl: "./order-information.component.html",
  styleUrls: ["./order-information.component.scss"],
})
export class OrderInformationComponent {
  deliveryInfo = DELIVERY_DATE_INFORMATION;
  paymentMethods = PAYMENT_METHODS;
  returnRequirements = RETURN_REQUIREMENTS;
  returnProcedure = RETURN_PROCEDURE;
  freeReturns = FREE_RETURNS;
}
