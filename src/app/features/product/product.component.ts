import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ADD_TO_ORDER } from "src/app/global_properties";
import { HttpService } from "src/app/shared/services/http.service";
import { OrderService } from "src/app/shared/services/order.service";
import { Product } from "../../shared/models/Product";
import { FACTS, QUALITY_FACTS } from "./constants";

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.scss"],
})
export class ProductComponent implements OnInit {
  facts = FACTS;
  qualityFacts = QUALITY_FACTS;
  addToOrderButton = ADD_TO_ORDER;
  quantity = 1;
  finalPrice: number;
  categoryName = "";
  id: number;
  product: Product;

  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    private orderService: OrderService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.categoryName = params["categoryName"];
      this.id = params["id"];
      this.httpService
        .getProduct(this.categoryName, this.id)
        .subscribe((product: Product) => {
          this.product = product;
          this.finalPrice = this.product.price;
        });
    });
  }

  setPrice(): void {
    this.finalPrice = this.product.price * this.quantity;
  }

  addToOrder(productId: number, quantity: number) {
    this.orderService.addToOrder(productId, quantity);
  }
}
