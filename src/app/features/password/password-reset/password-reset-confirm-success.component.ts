import { Component } from "@angular/core";

@Component({
  selector: "app-password-reset-confirm-success",
  templateUrl: "./password-reset-confirm-success.component.html",
  styleUrls: ["./password-reset-confirm-success.component.scss"],
})
export class PasswordResetConfirmSuccessComponent {}
