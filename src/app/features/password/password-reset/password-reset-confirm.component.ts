import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ResetPasswordModel } from "./ResetPasswordModel";
import { ApiError } from "src/app/core/exceptions/ApiError";
import {
  FIELD_VALIDATION,
  PASSWORD_VALIDATION,
  PATTERN_VALIDATION,
  RESET_PASSWORD_CONFIRM_SUCCESS_URL,
  PAGE_INACTIVE_URL,
} from "src/app/global_properties";
import { HttpService } from "src/app/shared/services/http.service";
import { StoreService } from "src/app/shared/services/store.service";
import { CONFIRM_RESET_SUBMIT_BUTTON, CONFIRM_RESET_TITLE } from "../constants";

@Component({
  selector: "app-password-reset-confirm",
  templateUrl: "./password-reset-confirm.component.html",
  styleUrls: ["./password-reset-confirm.component.scss"],
})
export class PasswordResetConfirmComponent implements OnInit {
  fieldValidation = FIELD_VALIDATION;
  passwordValidation = PASSWORD_VALIDATION;
  patternValidation = PATTERN_VALIDATION;
  title = CONFIRM_RESET_TITLE;
  button = CONFIRM_RESET_SUBMIT_BUTTON;

  resetFormGroup: FormGroup;
  resetPasswordForm = new ResetPasswordModel();
  resetPasswordToken: "";
  errors: ApiError[] = [];
  passwordIsValid = false;

  passwordPattern = this.storeService.storeData.passwordPattern;

  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    private storeService: StoreService
  ) {}

  ngOnInit(): void {
    this.resetFormGroup = new FormGroup({
      password: new FormControl("", [
        Validators.required,
        Validators.pattern(this.passwordPattern),
      ]),
      confirmedPassword: new FormControl("", Validators.required),
    });
    this.route.queryParams.subscribe((params) => {
      this.resetPasswordToken = params["token"];
    });
  }

  submit() {
    this.resetPasswordForm.password = this.resetFormGroup.value.password;
    this.resetPasswordForm.confirmedPassword =
      this.resetFormGroup.value.confirmedPassword;
    this.httpService
      .checkResetPasswordToken(this.resetPasswordToken, this.resetPasswordForm)
      .subscribe(
        () => {
          this.httpService.redirectToPage(RESET_PASSWORD_CONFIRM_SUCCESS_URL);
        },
        () => {
          this.httpService.redirectToPage(PAGE_INACTIVE_URL);
        }
      );
  }

  isPasswordValid(event: boolean): void {
    this.passwordIsValid = event;
  }
}
