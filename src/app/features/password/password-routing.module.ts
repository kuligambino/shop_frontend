import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PasswordResetConfirmSuccessComponent } from "./password-reset/password-reset-confirm-success.component";
import { PasswordResetConfirmComponent } from "./password-reset/password-reset-confirm.component";
import { PasswordForgetSuccessComponent } from "./password-forget/password-forget-success.component";
import { PasswordForgetComponent } from "./password-forget/password-forget.component";
import { PasswordComponent } from "./password.component";

const routes: Routes = [
  {
    path: "",
    component: PasswordComponent,
    children: [
      {
        path: "forget",
        component: PasswordForgetComponent,
      },
      {
        path: "forget/succeed",
        component: PasswordForgetSuccessComponent,
      },
      {
        path: "reset/confirm",
        component: PasswordResetConfirmComponent,
      },
      {
        path: "reset/succeed",
        component: PasswordResetConfirmSuccessComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PasswordRoutingModule {}
