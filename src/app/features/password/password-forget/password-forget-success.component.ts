import { Component, OnInit } from "@angular/core";
import {
  FORGET_PASSWORD_HEADER,
  FORGET_PASSWORD_TITLE,
  FORGET_PASSWORD_DESCRIPTION,
} from "src/app/global_properties";
import { DataService } from "src/app/shared/services/data.service";
import { of } from "rxjs";

@Component({
  selector: "app-password-forget-success",
  templateUrl: "./password-forget-success.component.html",
  styleUrls: ["./password-forget-success.component.scss"],
})
export class PasswordForgetSuccessComponent implements OnInit {
  header = FORGET_PASSWORD_HEADER;
  title = FORGET_PASSWORD_TITLE;
  description = FORGET_PASSWORD_DESCRIPTION;

  userEmail$ = of("");

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.userEmail$ = this.dataService.currentEmail;
  }
}
