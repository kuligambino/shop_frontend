import { Component, OnInit } from "@angular/core";
import { HttpService } from "../../../shared/services/http.service";
import { FormGroup } from "@angular/forms";
import { DataService } from "../../../shared/services/data.service";
import {
  EMAIL_FORMAT_VALIDATION,
  EMAIL_NOT_EXISTS_VALIDATION,
  FIELD_VALIDATION,
  FORGET_PASSWORD_DESCRIPTION,
  FORGET_PASSWORD_HEADER,
  RESET_PASSWORD_SUCCEED_URL,
  SEND_BUTTON,
} from "../../../global_properties";
import { ApiError } from "src/app/core/exceptions/ApiError";
import { FormService } from "../../../shared/services/form.service";
import { RequestResetPassword } from "./RequestResetPassword";

@Component({
  selector: "app-password-forget",
  templateUrl: "./password-forget.component.html",
  styleUrls: ["./password-forget.component.scss"],
})
export class PasswordForgetComponent implements OnInit {
  forgetPasswordTitle = FORGET_PASSWORD_HEADER;
  forgetPasswordMessage = FORGET_PASSWORD_DESCRIPTION;
  fieldValidation = FIELD_VALIDATION;
  emailValidation = EMAIL_FORMAT_VALIDATION;
  emailNotExistsValidation = EMAIL_NOT_EXISTS_VALIDATION;
  sendButton = SEND_BUTTON;

  forgetPasswordForm: FormGroup;
  reqResetPassword: RequestResetPassword;
  errors: ApiError[] = [];

  constructor(
    private httpService: HttpService,
    private dataService: DataService,
    private formService: FormService
  ) {}

  ngOnInit(): void {
    this.forgetPasswordForm = this.formService.initForgetPasswordForm();
  }

  submit() {
    this.reqResetPassword = this.forgetPasswordForm.value;
    this.httpService
      .sendResetPasswordForm(this.reqResetPassword)
      .subscribe(() => {
        this.dataService.changeEmail(this.reqResetPassword.userEmail);
        this.httpService.redirectToPage(RESET_PASSWORD_SUCCEED_URL);
      });
  }

  acceptRecaptcha() {
    this.forgetPasswordForm.value.recaptchaAccepted = true;
  }
}
