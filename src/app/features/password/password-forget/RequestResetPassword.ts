export interface RequestResetPassword {
  userEmail: string;
  recaptchaAccepted: boolean;
}
