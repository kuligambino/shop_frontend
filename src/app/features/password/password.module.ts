import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PasswordRoutingModule } from "./password-routing.module";
import { PasswordComponent } from "./password.component";
import { PasswordForgetComponent } from "./password-forget/password-forget.component";
import { PasswordForgetSuccessComponent } from "./password-forget/password-forget-success.component";
import { PasswordResetConfirmComponent } from "./password-reset/password-reset-confirm.component";
import { PasswordResetConfirmSuccessComponent } from "./password-reset/password-reset-confirm-success.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [
    PasswordComponent,
    PasswordForgetComponent,
    PasswordForgetSuccessComponent,
    PasswordResetConfirmComponent,
    PasswordResetConfirmSuccessComponent,
  ],
  imports: [CommonModule, PasswordRoutingModule, SharedModule],
  exports: [
    PasswordComponent,
    PasswordForgetComponent,
    PasswordForgetSuccessComponent,
    PasswordResetConfirmComponent,
    PasswordResetConfirmSuccessComponent,
  ],
})
export class PasswordModule {}
