import { Component } from "@angular/core";
import { OwlOptions } from "ngx-owl-carousel-o";
import { ADS } from "../constants";

@Component({
  selector: "app-ad-carousel",
  templateUrl: "./ad-carousel.component.html",
  styleUrls: ["./ad-carousel.component.scss"],
})
export class AdCarouselComponent {
  ads = ADS;

  customOptions: OwlOptions = {
    items: 1,
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    dotsSpeed: 700,
    margin: 0,
    stagePadding: 0,
    nav: false,
    responsive: {
      1280: {
        items: 1,
      },
    },
  };
}
