import { Component, Input } from "@angular/core";
import { OwlOptions } from "ngx-owl-carousel-o";
import { ADD_TO_ORDER } from "../../../global_properties";
import { OrderService } from "../../../shared/services/order.service";
import { Product } from "../../../shared/models/Product";

@Component({
  selector: "app-product-carousel",
  templateUrl: "./product-carousel.component.html",
  styleUrls: ["./product-carousel.component.scss"],
})
export class ProductCarouselComponent {
  addToOrderButton = ADD_TO_ORDER;
  @Input() products: Product[] = [];
  @Input() title = "";

  customOptions: OwlOptions = {
    items: 5,
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 400,
    margin: 50,
    stagePadding: 50,
    navText: ["<-", "->"],
    nav: true,
    slideBy: 3,
  };

  constructor(private orderService: OrderService) {}

  addToOrder(productId: number): void {
    this.orderService.addToOrder(productId);
  }
}
