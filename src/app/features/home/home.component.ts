import { Component, OnInit } from "@angular/core";
import { HttpService } from "src/app/shared/services/http.service";
import { Product } from "../../shared/models/Product";
import { getKeys, getValues } from "../../core/header/home-helper";
import { CHEAPEST, MOST_EXPENSIVE, SPECIALS, TASTY } from "./constants";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  specials = SPECIALS;
  cheapest = CHEAPEST;
  mostExpensive = MOST_EXPENSIVE;
  tasty = TASTY;

  prod: Map<string, Product[]> = new Map();
  keys: string[] = [];

  constructor(private httpService: HttpService) {}

  ngOnInit(): void {
    this.httpService
      .getProducts()
      .subscribe((products: Map<string, Product[]>) => {
        this.prod = products;
        this.keys = getKeys(products);
      });
  }

  getValuesFromMap(key: string): Product[] {
    return getValues(key, this.prod);
  }
}
