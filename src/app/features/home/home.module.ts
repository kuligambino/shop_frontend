import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CarouselModule } from "ngx-owl-carousel-o";

import { HomeRoutingModule } from "./home-routing.module";
import { AdCarouselComponent } from "./components/ad-carousel.component";
import { ProductCarouselComponent } from "./components/product-carousel.component";
import { HomeComponent } from "./home.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [AdCarouselComponent, HomeComponent, ProductCarouselComponent],
  imports: [CarouselModule, CommonModule, HomeRoutingModule, SharedModule],
  exports: [AdCarouselComponent, HomeComponent, ProductCarouselComponent],
})
export class HomeModule {}
