import { Component } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import {
  DOWNLOAD_FILE_MESSAGE,
  MESSAGE_COOKIE,
  TITLE_COOKIE,
} from "src/app/core/constants";
import { ACCEPT_BUTTON, COOKIE_LAW_NAME } from "../../global_properties";

@Component({
  selector: "app-cookie-law",
  templateUrl: "./cookie-law.component.html",
  styleUrls: ["./cookie-law.component.scss"],
})
export class CookieLawComponent {
  title = TITLE_COOKIE;
  message = MESSAGE_COOKIE;
  downloadMessage = DOWNLOAD_FILE_MESSAGE;
  acceptButton = ACCEPT_BUTTON;
  private cookieLawSeen = false;

  constructor(private cookieService: CookieService) {}

  acceptCookiesPolicy(): void {
    this.cookieLawSeen = true;
    this.cookieService.set(COOKIE_LAW_NAME, this.cookieLawSeen.toString());
  }

  get getCookieLawSeen(): boolean {
    return this.cookieLawSeen;
  }
}
