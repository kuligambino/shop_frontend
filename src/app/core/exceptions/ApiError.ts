export class ApiError {
  constructor(
    public field: string,
    public code: string,
    public message: string
  ) {}
}
