import { Injectable } from "@angular/core";
import { merge, Observable } from "rxjs";
import { switchMap } from "rxjs/operators";
import { HttpService } from "../../../shared/services/http.service";
import { QuantityUpdateService } from "../../../shared/services/quantity-update.service";

@Injectable()
export class CartQuantityService {
  constructor(
    private httpService: HttpService,
    private quantityUpdateService: QuantityUpdateService
  ) {}

  getCartQuantity(): Observable<number> {
    return merge(
      this.getCurrentQuantity(),
      this.quantityUpdateService
        .getUpdateQuantity()
        .pipe(switchMap(() => this.getCurrentQuantity()))
    );
  }

  private getCurrentQuantity(): Observable<number> {
    return this.httpService.getCurrentOrderProductsQuantity();
  }
}
