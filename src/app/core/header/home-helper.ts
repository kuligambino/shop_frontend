import { Product } from "../../shared/models/Product";

export function getKeys(map: Map<string, Product[]>): string[] {
  return Object.keys(map);
}

export function getValues(
  key: string,
  prod: Map<string, Product[]>
): Product[] {
  return Object.entries(prod)
    .filter((products) => products[0] === key)
    .map((p) => p[1])
    .flat();
}
