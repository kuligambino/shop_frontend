import { Component, OnInit } from "@angular/core";
import {
  ABOUT_US,
  CONTACT,
  REGISTER,
  SHOP_NAME,
  SHOPPING_CART,
} from "src/app/core/constants";
import { AuthService } from "../../shared/services/auth.service";
import { DataService } from "../../shared/services/data.service";
import { of } from "rxjs";
import { HOME, LOGIN } from "../../global_properties";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
  shopName = SHOP_NAME;
  home = HOME;
  aboutUs = ABOUT_US;
  contact = CONTACT;
  login = LOGIN;
  cart = SHOPPING_CART;
  register = REGISTER;
  showAddedProductDialog$ = of({});

  public isMenuCollapsed = true;

  constructor(
    private authService: AuthService,
    private dataService: DataService
  ) {}

  ngOnInit(): void {
    this.showAddedProductDialog$ = this.dataService.addedProduct;
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }
}
