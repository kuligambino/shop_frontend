import { Component, OnInit } from "@angular/core";
import { REGISTER, SHOPPING_CART } from "src/app/core/constants";
import { Observable, of } from "rxjs";
import { CartQuantityService } from "../services/cart-quantity.service";
import { LOGIN } from "../../../global_properties";

@Component({
  selector: "app-not-logged-header-content",
  templateUrl: "./not-logged-header-content.component.html",
  styleUrls: ["./not-logged-header-content.component.scss"],
})
export class NotLoggedHeaderContentComponent implements OnInit {
  login = LOGIN;
  cart = SHOPPING_CART;
  register = REGISTER;
  quantity$: Observable<number> = of(0);

  constructor(private cartQuantityService: CartQuantityService) {}

  ngOnInit(): void {
    this.quantity$ = this.cartQuantityService.getCartQuantity();
  }
}
