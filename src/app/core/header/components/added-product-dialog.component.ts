import { Component, OnInit } from "@angular/core";
import { DataService } from "../../../shared/services/data.service";
import { of } from "rxjs";

@Component({
  selector: "app-added-product-dialog",
  templateUrl: "./added-product-dialog.component.html",
  styleUrls: ["./added-product-dialog.component.scss"],
})
export class AddedProductDialogComponent implements OnInit {
  product$ = of({});

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.product$ = this.dataService.addedProduct;
    setTimeout(
      function () {
        this.dataService.addProduct({});
      }.bind(this),
      1500
    );
  }
}
