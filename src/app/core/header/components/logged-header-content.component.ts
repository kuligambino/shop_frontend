import { Component } from "@angular/core";
import { LOGOUT, MY_ACCOUNT, SHOPPING_CART } from "src/app/core/constants";
import { AuthService } from "../../../shared/services/auth.service";
import { Observable, of } from "rxjs";
import { CartQuantityService } from "../services/cart-quantity.service";

@Component({
  selector: "app-logged-header-content",
  templateUrl: "./logged-header-content.component.html",
  styleUrls: ["./logged-header-content.component.scss"],
})
export class LoggedHeaderContentComponent {
  cart = SHOPPING_CART;
  myAccount = MY_ACCOUNT;
  logout = LOGOUT;
  quantity$: Observable<number> = of(0);

  constructor(
    private cartQuantityService: CartQuantityService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.quantity$ = this.cartQuantityService.getCartQuantity();
  }

  logoutUser() {
    this.authService.logout();
  }
}
