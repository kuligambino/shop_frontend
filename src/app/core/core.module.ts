import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { NotFoundComponent } from "./not-found/not-found.component";
import { FooterComponent } from "./footer/footer.component";
import { UpsComponent } from "./footer/ups/ups.component";
import { HeaderComponent } from "./header/header.component";
import { CookieLawComponent } from "./cookie-law/cookie-law.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { CoreRoutingModule } from "./core-routing.module";
import { LoggedHeaderContentComponent } from "./header/components/logged-header-content.component";
import { NotLoggedHeaderContentComponent } from "./header/components/not-logged-header-content.component";
import { AddedProductDialogComponent } from "./header/components/added-product-dialog.component";
import { SharedModule } from "../shared/shared.module";
import { CartQuantityService } from "./header/services/cart-quantity.service";

@NgModule({
  declarations: [
    CookieLawComponent,
    UpsComponent,
    FooterComponent,
    HeaderComponent,
    NotFoundComponent,
    LoggedHeaderContentComponent,
    NotLoggedHeaderContentComponent,
    AddedProductDialogComponent,
  ],
  exports: [
    CookieLawComponent,
    UpsComponent,
    FooterComponent,
    HeaderComponent,
    NotFoundComponent,
  ],
  imports: [NgbModule, CommonModule, CoreRoutingModule, SharedModule],
  providers: [CartQuantityService],
})
export class CoreModule {}
