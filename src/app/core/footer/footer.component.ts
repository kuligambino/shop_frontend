import { Component } from "@angular/core";
import { FOOTER } from "../constants";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"],
})
export class FooterComponent {
  footer = FOOTER;
}
