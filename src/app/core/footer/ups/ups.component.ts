import { Component } from "@angular/core";
import { LOGO, UPS } from "../../constants";

@Component({
  selector: "app-ups",
  templateUrl: "./ups.component.html",
  styleUrls: ["./ups.component.scss"],
})
export class UpsComponent {
  readonly logo = LOGO;
  readonly ups = UPS;
}
