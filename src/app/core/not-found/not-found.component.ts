import { Component } from "@angular/core";
import {
  CONTACT,
  PAGE_NOT_FOUND,
  PAGE_NOT_FOUND_MESSAGE,
} from "src/app/core/constants";
import { HOME } from "../../global_properties";

@Component({
  selector: "app-not-found",
  templateUrl: "./not-found.component.html",
  styleUrls: ["./not-found.component.scss"],
})
export class NotFoundComponent {
  pageNotFound = PAGE_NOT_FOUND;
  pageNotFoundMessage = PAGE_NOT_FOUND_MESSAGE;
  home = HOME;
  contact = CONTACT;
}
