export const PAGE_NOT_FOUND_MESSAGE = "Oops! HTTP 404";
export const SHOP_NAME = "Supply shop";
export const ABOUT_US = "O nas";
export const CONTACT = "Kontakt";
export const SHOPPING_CART = "Koszyk ";
export const REGISTER = "Dołącz do nas ";
export const MY_ACCOUNT = "Moje konto ";
export const LOGOUT = "Wyloguj się ";
export const FOOTER = "© 2020 Copyright: KKWJ Company";
export const TITLE_COOKIE = "ZASTOSOWANIE PLIKÓW COOKIE";
export const MESSAGE_COOKIE =
  "Używamy plików cookie, aby ułatwić Ci surfowanie po Internecie i aby udoskonalać naszą witrynę.\n" +
  "            Korzystając z naszej strony internetowej wyrażasz zgodę na zastosowanie plików cookie.";
export const DOWNLOAD_FILE_MESSAGE =
  "Aby uzyskać\n" + "            więcej informacji, kliknij tutaj";
export const PAGE_NOT_FOUND = "Nie znaleziono strony!";
export const UPS = [
  [
    "/assets/ups1.png",
    "POLSKA FIRMA",
    "Robimy produkty KKWJ z pasją. Kupujesz bez pośredników!",
  ],
  [
    "/assets/ups2.png",
    "GWARANCJA JAKOŚCI",
    "Stawiamy na jakość dostarczając najlepsze narzędzia do wsparcia Twojego treningu.",
  ],
  [
    "/assets/ups3.png",
    "PUNKTY KKWJ ZA ZAKUPY",
    "Każdy zakup to punkty dla Ciebie na następne zakupy i rabaty.",
  ],
  [
    "/assets/ups4.png",
    "GWARANCJA BEZPIECZEŃSTWA",
    "Certyfikat SSL zabezpieczający płatności.",
  ],
];
export const LOGO = ["/assets/ups5.jpg", "KUPUJĄC U NAS:"];
